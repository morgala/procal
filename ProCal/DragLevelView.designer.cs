// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("DragLevelView")]
	partial class DragLevelView
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView arrowImageView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (arrowImageView != null) {
				arrowImageView.Dispose ();
				arrowImageView = null;
			}
		}
	}
}
