using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MTiRate;

namespace ProCal
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		static AppDelegate ()
		{
			iRate.SharedInstance.DaysUntilPrompt = 5;
			iRate.SharedInstance.UsesUntilPrompt = 5;
			iRate.SharedInstance.PromptAgainForEachNewVersion = true;
			iRate.SharedInstance.AppStoreID = 830744947;

		}


		// class-level declarations
		public override UIWindow Window {
			get;
			set;
		}

		public override bool FinishedLaunching (UIApplication application, NSDictionary launchOptions)
		{
			bool isRootVSSet = false;
			application.ApplicationSupportsShakeToEdit = true;
			application.ApplicationIconBadgeNumber = 0;

			//TODO: need to check if user is logged in.
			// check for a notification
			if(launchOptions != null) {

				// check for a local notification
				if(launchOptions.ContainsKey(UIApplication.LaunchOptionsLocalNotificationKey)) {

					UILocalNotification localNotification = launchOptions[UIApplication.LaunchOptionsLocalNotificationKey] as UILocalNotification;
					if(localNotification != null) {

						new UIAlertView(localNotification.AlertAction, localNotification.AlertBody, null, "OK", null).Show();
						if(Window.RootViewController != null)
						{
							// if rootviewcontroller is setup then change to tab
							if(Window.RootViewController is ProCalViewController)
							{
								Window.RootViewController = (UIViewController) Window.RootViewController.Storyboard.InstantiateViewController("mainTabBarController");
								isRootVSSet = true;
							}

							// display the calendar tab
							UITabBarController tabBar = (UITabBarController)Window.RootViewController;
							tabBar.SelectedIndex = 1;
						}
					}
				}
			}

			// if user is logged in, set the loadingViewController to rootviewcontroller
			// ensure the viewcontroller in xcode has a storyboard id - DREAM!
			if(ProCalManager.Instance.User != null )
			{
				// schedule notifications
				if (ProCalManager.Instance.User.NotificationsEnabled)
				{
					ProCalManager.Instance.User.NotificationsEnabled = true;
				}
					 
				if (!isRootVSSet)
				{
					Window.RootViewController = (UIViewController)Window.RootViewController.Storyboard.InstantiateViewController ("loadingViewController");
				}
			}

			return true;
		}

		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
			ProCalManager.Instance.ResignTimeStamp = DateTime.Now;
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = (int)ProCalManager.Instance.User.TodaysTotalProteinIntake;
		}
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
		}

		/// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
			// reset notifications.
			var user = ProCalManager.Instance.User;
			if (user != null && user.NotificationsEnabled) {
				double percentTotal = user.TodaysTotalProteinIntake / user.DailyProteinIntakeLimit;
				if (percentTotal < 1) {
					user.NotificationsEnabled = true;
				}
			}
		}

		/// This method is called when the application is about to terminate. Save data, if needed. 
		public override void WillTerminate (UIApplication application)
		{
		}
	}
}

