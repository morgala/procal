// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("ActivityLevelView")]
	partial class ActivityLevelView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton heavyButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton lightButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton modButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton sedButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton veryActiveButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (sedButton != null) {
				sedButton.Dispose ();
				sedButton = null;
			}

			if (heavyButton != null) {
				heavyButton.Dispose ();
				heavyButton = null;
			}

			if (modButton != null) {
				modButton.Dispose ();
				modButton = null;
			}

			if (lightButton != null) {
				lightButton.Dispose ();
				lightButton = null;
			}

			if (veryActiveButton != null) {
				veryActiveButton.Dispose ();
				veryActiveButton = null;
			}
		}
	}
}
