using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Back2Front.GUI;

namespace ProCal
{
	public partial class FoodPickerController : UIViewController
	{
		private FoodPickerTableSource _dataSource;
		private int _charCount;
		private UIView _backgroundModalView;
		private AddFoodView _addFoodView;

		public event EventHandler<EventArgs> Close;

		public FoodPickerController () : base ("FoodPickerController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			backgroundImageView.Image = ProCalManager.Instance.User.Sex == Sex.Male ? UIImage.FromFile ("bg.png") : UIImage.FromFile ("bg-female.png");

			ProCalManager.Instance.FoodUpdated += (sender, e) => {
				foodTableView.ReloadData ();
			};
			
			closeButton.TouchUpInside += (sender, e) => {
				if(Close != null)
				{
					// clean up dummy food objects;
					ProCalManager.Instance.AllFoods.RemoveAll(f => f.IsDummy);
					Close(this, null);
				}
			};
			addFoodButton.TouchUpInside += AddFoodButtonOnTouch;

			_dataSource = new FoodPickerTableSource(ProCalManager.Instance.AllFoods, foodTableView);
			foodTableView.Source = _dataSource;
			searchBar.OnEditingStarted += (sender, e) => {
				_dataSource.Reset();
			};
			searchBar.TextChanged += HandleTextChanged;
			searchBar.SearchButtonClicked += (sender, e) => searchBar.ResignFirstResponder ();

			// used to dismiss keyboard for textfields
			var tap = new UITapGestureRecognizer ();
			tap.CancelsTouchesInView = false;
			tap.AddTarget (() =>{
				View.EndEditing (true);
			});
			foodTableView.AddGestureRecognizer (tap);
		}

		private void AddFoodButtonOnTouch(object sender, EventArgs e)
		{
			_dataSource.Reset ();
			_addFoodView = AddFoodView.GetView ();
			_addFoodView.Frame = new RectangleF((UIScreen.MainScreen.Bounds.Width - _addFoodView.Frame.Width)/2 , (UIScreen.MainScreen.Bounds.Height - _addFoodView.Frame.Height)/2, _addFoodView.Frame.Width, _addFoodView.Frame.Height);
			_backgroundModalView = new UIView(View.Bounds);
			_backgroundModalView.BackgroundColor = UIColor.Black.ColorWithAlpha (0.2f);
			View.UserInteractionEnabled = false;
			UIApplication.SharedApplication.KeyWindow.AddSubview(_backgroundModalView);
			UIApplication.SharedApplication.KeyWindow.AddSubview((UIView)_addFoodView);
			_addFoodView.Animate();
			_addFoodView.Close += (s, ev) => {
				_backgroundModalView.RemoveFromSuperview ();
				((UIView)_addFoodView).RemoveFromSuperview ();
				_backgroundModalView = null;
				_addFoodView = null;
				View.UserInteractionEnabled = true;

				// reload table.
				if(ev.HasNewFoodBeenCreated)
				{
					foodTableView.ReloadData();
				}
			};
		}

		private void HandleTextChanged (object sender, UISearchBarTextChangedEventArgs e)
		{
			if(string.IsNullOrEmpty (e.SearchText))
			{
				_dataSource.Data = ProCalManager.Instance.AllFoods;
				foodTableView.ReloadData();
				return;
			}

			List<Food> foods;

			if (e.SearchText.Length < 3) {
				return;
			}
			if(e.SearchText.Length == 3)
			{
				// one char so load all cities.
				foods = ProCalManager.Instance.AllFoods;
			}
			else
			{
				if(_charCount > e.SearchText.Length)
				{
					foods = ProCalManager.Instance.AllFoods;
				}
				else
				{
					// filter down using last search results
					foods = _dataSource.Data;
				}
			}

			_charCount = e.SearchText.Length;
			var searchFilter = e.SearchText.ToLower();
			try
			{
				var list = foods.Where (f => !f.IsDummy && f.Name.ToLower ().Contains (searchFilter)).OrderBy (c => c.Name);
				_dataSource.Data = list.ToList();
				foodTableView.ReloadData();
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				throw;
			}
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			View.EndEditing(true);
		}
	}

	public class FoodPickerTableSource : UITableViewSource
	{
		private int _detailRowIndex = -1;
		private int _currentRow = -1;
		private int _outOfScopeRow = -1;
		private UITableView _tableView;

		public FoodPickerTableSource(List<Food> data, UITableView tableView)
		{
			Data = data;
			_tableView = tableView;
		}

		public List<Food> Data{get;set;}

		public void Reset()
		{
			if (_currentRow != -1) {
				RowSelected (_tableView, NSIndexPath.FromRowSection (_currentRow, 0));
			}
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return Data.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			int rowIndex = indexPath.Row;
			bool isDetailRow = _detailRowIndex != -1 && rowIndex == _detailRowIndex;

			UITableViewCell cell = tableView.DequeueReusableCell (isDetailRow ? FoodItemDetailCell.Key.ToString () : FoodItemHeaderCell.Key.ToString ());

			if (cell == null) {
				if (isDetailRow) {
					cell = FoodItemDetailCell.Create ();
					((FoodItemDetailCell)cell).Initialise ();
				} else {
					cell = FoodItemHeaderCell.Create ();
				}
			}
			if (isDetailRow) {
				((FoodItemDetailCell)cell).Food = Data [rowIndex - 1];
			} else {
				if (((FoodItemHeaderCell)cell).IsSelected)
				{
					cell = FoodItemHeaderCell.Create ();
				}
				if (_currentRow == indexPath.Row) {
					HighLightRow ((FoodItemHeaderCell)cell);
				}
				if (_currentRow == _outOfScopeRow) { 
					_outOfScopeRow = -1;
					UnhighlightRow ((FoodItemHeaderCell)cell);
				}
				((FoodItemHeaderCell)cell).Food = Data [rowIndex];
			}
			return cell;
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return indexPath.Row == _detailRowIndex ? 225f : 44f;
		}

		public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{
			var headerCell = tableView.CellAt(indexPath) as FoodItemHeaderCell;
			if (headerCell != null) {
				UnhighlightRow (headerCell);
			}
			else
			{
				// if we are the cell is out of vision
				_outOfScopeRow = indexPath.Row;
			}
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if(_currentRow == -1)
			{
				_currentRow = indexPath.Row;
			}
			else
			{
				// user touched detail row
				if(_currentRow == indexPath.Row || _detailRowIndex == indexPath.Row)
				{
					if(_detailRowIndex != -1)
					{
						Data.RemoveAt(_detailRowIndex);
						int idxToRemove = _detailRowIndex;
						_detailRowIndex = -1;
						tableView.DeleteRows(new NSIndexPath[]{NSIndexPath.FromRowSection(idxToRemove, 0)}, UITableViewRowAnimation.Fade);
						_outOfScopeRow = _currentRow;
						_currentRow = -1;
						//_isSelected = true;
						tableView.ReloadRows (new[]{ indexPath }, UITableViewRowAnimation.Automatic);
						return;
					}
				
				}
			}

			if(_detailRowIndex != -1)
			{
				// at this point we remove the detail and un
				Data.RemoveAt(_detailRowIndex);
				tableView.DeleteRows(new NSIndexPath[]{NSIndexPath.FromRowSection(_detailRowIndex, 0)}, UITableViewRowAnimation.Fade);
			}

			if(indexPath.Row > _detailRowIndex && _detailRowIndex != -1)
			{
				_detailRowIndex = indexPath.Row;
			}
			else
			{
				_detailRowIndex = indexPath.Row+1;
			}
			_currentRow = _detailRowIndex-1;

			var headerCell = tableView.CellAt(NSIndexPath.FromRowSection(_currentRow,0)) as FoodItemHeaderCell;
			if (headerCell != null) {
				HighLightRow (headerCell);
			}

			Data.Insert(_detailRowIndex, new Food{IsDummy = true});
			tableView.InsertRows(new NSIndexPath[]{NSIndexPath.FromRowSection(_detailRowIndex, 0)}, UITableViewRowAnimation.Automatic);
		}

		private void HighLightRow(FoodItemHeaderCell cell)
		{
			cell.BackgroundColor = UIColor.White;//.ColorWithAlpha(0.5f);
			cell.FoodLabel.TextColor = UIColor.Black;
			cell.ProteinLabel.TextColor = UIColor.Black;
			cell.IsSelected = true;
		}

		private void UnhighlightRow(FoodItemHeaderCell cell, bool isSelected=false)
		{
			cell.BackgroundColor = UIColor.Clear;
			cell.FoodLabel.TextColor = UIColor.White;
			cell.ProteinLabel.TextColor = UIColor.White;
			cell.IsSelected = isSelected;
		}
	}
}

