using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;
using MonoTouch.CoreGraphics;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Back2Front.Animation;
using Back2Front.GUI;
using Back2Front.Utilities;

namespace ProCal
{
	public class CalendarWeekView : UIView
	{
		public Action<DateTime> OnDateSelected;
		public Action<DateTime> OnFinishedDateSelection;
		public Func<DateTime, bool> IsDayMarkedDelegate;
		public Func<DateTime, bool> IsDateAvailable;
		private UIView _scrollView;
		private WeekGridView _weekGridView;
		private UILabel _dateLabel;
		private UIPanGestureRecognizer _panGest;
		private float _originalPosition;
		private PointF _scrollViewCentre;
		private UIButton _leftButton;
		private UIButton _rightButton;

		public bool CalendarIsLoaded{get;set;}
		public DateTime CurrentDate { get; set; }
		public bool IsLocked{get;private set;}
		public bool ShowWeeklyText{ get; private set; }
		public bool ShowNavButtons{get;set;}
		public bool AnimateView{ get; set;}

		public DateTime SelectedDate
		{
			get{
				return _weekGridView.SelectedDayView.Date;
			}
		}

		public CalendarWeekView(RectangleF frame) : this(frame, false, false, true)
		{
		}

		public CalendarWeekView(RectangleF frame, bool isLocked, bool showWeeklyText, bool showNavButtons=false) : base(frame)
		{
			CurrentDate = DateTime.Now.Date;
			IsLocked = isLocked;
			ShowWeeklyText = showWeeklyText;
			ShowNavButtons = showNavButtons;
		}

		public override void SetNeedsDisplay ()
		{
			base.SetNeedsDisplay();
			if (_weekGridView!=null)
				_weekGridView.Update();
		}

		public override void LayoutSubviews ()
		{
			if (CalendarIsLoaded) return;

 			_scrollView = new UIView (new RectangleF (0, 44, 320, 55));			
			_scrollViewCentre = _scrollView.Center;
		
			InitialiseDateLabel();
			LoadButtons ();
			LoadInitialGrids();

			BackgroundColor = UIColor.Clear;
			AddSubview(_scrollView);
			_scrollView.AddSubview(_weekGridView);
			CalendarIsLoaded = true;

			if (AnimateView) {
				Animate ();
			}

			if(IsLocked)
			{
				return;
			}
			_panGest = new UIPanGestureRecognizer (() => {
				if(_panGest.State == UIGestureRecognizerState.Began)
				{
					_originalPosition = _scrollView.Frame.Location.X;
				}
				else if(_panGest.State == UIGestureRecognizerState.Changed)
				{
					var movement = _panGest.TranslationInView(_scrollView);
					var newPosition = new PointF(movement.X + _originalPosition, 44);
					_scrollView.Frame = new RectangleF(newPosition, _scrollView.Frame.Size);
				}
				else if(_panGest.State == UIGestureRecognizerState.Ended)
				{
					var loc = _panGest.LocationInView(this);
					if(loc.X > 60 && loc.X < 260)
					{
						UIView.Animate (0.4, 0.0, UIViewAnimationOptions.CurveEaseInOut, () => {
							_scrollView.Center = _scrollViewCentre;
						}, null);
					}
					else if(loc.X >=260)
					{

						MoveCalendarMonths(-7);
					}
					else
					{
						bool didMove = MoveCalendarMonths(7);
						if(!didMove)
						{
							// this is in the future so animate scroll view back to position
							UIView.Animate (0.4, 0.0, UIViewAnimationOptions.CurveEaseInOut, () => {
								_scrollView.Center = _scrollViewCentre;
							}, null);
						}
					}
				}
			});
			_scrollView.AddGestureRecognizer (_panGest);
		}



		public void DeselectDate(){
			if (_weekGridView!=null)
				_weekGridView.DeselectDayView();
		}

		private void LoadButtons()
		{
			if (!ShowNavButtons) {
				return;
			}

			_leftButton = UIButton.FromType(UIButtonType.Custom);
			_leftButton.TouchUpInside += HandlePreviousMonthTouch;
			_leftButton.SetImage(UIImage.FromFile("calendar-left.png"), UIControlState.Normal);
			_leftButton.Frame = new RectangleF(10, 5, 25, 25);

			_rightButton = UIButton.FromType(UIButtonType.Custom);
			_rightButton.TouchUpInside += HandleNextMonthTouch;
			_rightButton.SetImage(UIImage.FromFile("calendar-right.png"), UIControlState.Normal);
			_rightButton.Frame = new RectangleF(320 - 35, 5, 25, 25);

			AddSubview (_leftButton);
			AddSubview (_rightButton);
			_rightButton.Hidden = CurrentDate.IsInCurrentWeek ();
		}

		private void InitialiseDateLabel()
		{
			_dateLabel = new UILabel (new RectangleF(new PointF(0, 0), new SizeF {Width = 320, Height = 30}));
			_dateLabel.TextColor = UIColor.White;
			_dateLabel.TextAlignment = UITextAlignment.Center;
			AddSubview (_dateLabel);
			if (IsLocked)
			{
				if (ShowWeeklyText) {
					_dateLabel.Text = "Weekly Protein Tracker";
				} else {
					_dateLabel.Text = DateTime.Now.ToString("ddd dd MMMM yyyy");
				}
			}
			else
			{
				_dateLabel.Text = CurrentDate.ToString("ddd dd MMMM yyyy");
			}
		}

		private void HandlePreviousMonthTouch(object sender, EventArgs e)
		{
			MoveCalendarMonths(-7);
		}
		private void HandleNextMonthTouch(object sender, EventArgs e)
		{
			MoveCalendarMonths(7);
		}

		private void FireOnMonthChanged()
		{
			if (OnDateSelected != null)
			{
				OnDateSelected (_weekGridView.SelectedDayView.Date);
			}
		}

		public void RefreshWeek()
		{
			if (_weekGridView == null) {
				return;
			}
			_weekGridView.RefreshGrid ();
			var dayView = _weekGridView.DayViews.Find(d => d.Date == CurrentDate);
			if (dayView != null) {
				dayView.Selected = true;
				_weekGridView.SelectedDayView = dayView;
				//ReloadView ();
				if (OnDateSelected != null) {
					OnDateSelected (_weekGridView.SelectedDayView.Date);
				}
			}
		}

		public void RefreshCurrentDate()
		{
			_weekGridView.RefreshDayView ();
		}

		public void Animate()
		{
			if (_weekGridView == null) {
				return;
			}

			double delay = 0.05;
			int numViews = _weekGridView.DayViews.Count;
			_weekGridView.DayViews.ForEach ( d => {
				delay += 0.05;
				d.Alpha = 0;
				UIView.Animate(0.3,delay, UIViewAnimationOptions.CurveLinear, ()=>{
					d.Alpha = 1;
				},() =>{
					numViews--;
					if(numViews == 1)
					{
						AnimateTodaysView();
					}
				});
			});
		}

		private void AnimateTodaysView()
		{
			var dayView = _weekGridView.DayViews.Find(d => d.Date == DateTime.Today);
			if(dayView == null)
			{
				return;
			}
			CATransform3D endTransform = dayView.Layer.Transform;
			CATransform3D startTransform = endTransform.Scale (0.6f,0.6f,0.6f);
			var transform = new TransformBounceAnimation("transformKey", dayView)
			{
				FromValue = NSValue.FromCATransform3D (startTransform),
				ToValue = NSValue.FromCATransform3D (endTransform),
				Duration = 1f,
				NumberOfBounces = 4,
				ShouldOvershoot = true
			};
			transform.AnimationCompleted += (sender, e) => {
				if (dayView.ProteinAward == ProteinAward.Gold) {
					dayView.Glow ();
				}
			};
			transform.Initialise();
			dayView.Layer.AddAnimation(transform.Animation, transform.Key);
		}

		public void MoveToToday(bool animate = false)
		{
			if (_weekGridView == null) {
				return;
			}
			int days = 0;
			// need to handle year movement
			if(DateTime.Now.Year != CurrentDate.Year)
			{
				if(CurrentDate > DateTime.Now)
				{
					days = CurrentDate.Subtract(DateTime.Now).Days;
				}
				else
				{
					days = DateTime.Now.Subtract(CurrentDate).Days;
				}
			}
			else
			{
				days = DateTime.Now.Subtract(CurrentDate).Days;
			}

			if(days != 0 && !CurrentDate.IsInCurrentWeek())
			{
				MoveCalendarMonths(days);
			}
			else
			{
				_weekGridView.DayViews.ForEach(d => {
					if(d.Selected)
					{
						d.Selected = false;
					}
				});

				var dayView = _weekGridView.DayViews.Find(d => d.Date == DateTime.Today);
				if (dayView != null) {
					CurrentDate = dayView.Date;
					dayView.Selected = true;
					dayView.Available = true;
					dayView.Active = dayView.Date <= DateTime.Today;
					if (!IsLocked) {
						dayView.Alpha = 0;
						UIView.Animate (0.2, 0, UIViewAnimationOptions.CurveLinear, () => {
							dayView.Alpha = 1;
						}, null);
					}
					_weekGridView.SelectedDayView = dayView;
					UpdateDateLabel (dayView.Date);
					if (OnDateSelected != null) {
						OnDateSelected (_weekGridView.SelectedDayView.Date);
					}
				}
				if (animate) {
					AnimateTodaysView ();
				}
			}
		}

		public bool MoveCalendarMonths(int days)
		{
			var newDate = CurrentDate.AddDays (days);
			// ensure we can't go into the future
			if (newDate > DateTime.Today) {
				if (!newDate.IsInCurrentWeek ()) {
					return false;
				}
				CurrentDate = DateTime.Today;
			} else {
				CurrentDate = newDate;
			}


			UserInteractionEnabled = false;
			UpdateDateLabel (CurrentDate);

			_rightButton.Hidden = newDate.IsInCurrentWeek ();

			var gridToMove = CreateNewGrid(CurrentDate);
			var pointsToMove = days > 0 ? 320 : -320;

			gridToMove.Frame = new RectangleF(new PointF(pointsToMove*2, 5), gridToMove.Frame.Size);
			_scrollView.AddSubview(gridToMove);

			UIView.Animate (0.4, 0.1, UIViewAnimationOptions.CurveEaseInOut, () => {

				_weekGridView.Alpha = 0;
				_weekGridView.Center = new PointF(_weekGridView.Center.X - pointsToMove, _weekGridView.Center.Y);
				_scrollView.Center = _scrollViewCentre;
				gridToMove.Center = new PointF(_scrollViewCentre.X, gridToMove.Center.Y);
			}, null);

			SetNeedsDisplay();
			_weekGridView = gridToMove;
			UserInteractionEnabled = true;
			FireOnMonthChanged();
			return true;
		}

		private WeekGridView CreateNewGrid(DateTime date){
			var grid = new WeekGridView(this, date);
			grid.CurrentDate = CurrentDate;
			grid.BuildGrid();
			grid.Frame = new RectangleF(0, 5, 320, 44);
			return grid;
		}

		private void LoadInitialGrids()
		{
			_weekGridView = CreateNewGrid(CurrentDate);
			if (!IsLocked) {
				FireOnMonthChanged ();
			}
		}

		public override void Draw(RectangleF rect)
		{
			DrawDayLabels(rect);
		}

		public void UpdateDateLabel(DateTime date)
		{
			if (!CalendarIsLoaded) {
				return;
			}

			if (ShowWeeklyText) {
				return;
			}

			if (IsLocked)
			{
				_dateLabel.Text = date.ToString ("ddd dd MMMM yyyy");
			} 
			else 
			{

				_dateLabel.Alpha = 0;
				_dateLabel.Text = date.ToString ("ddd dd MMMM yyyy");
				UIView.Animate (0.2, 0.1, UIViewAnimationOptions.CurveLinear, () => {
					_dateLabel.Alpha = 1;
				}, null);
			}

		}

		private void DrawDayLabels(RectangleF rect)
		{
			var font = SystemFont.SystemFontOfSize (14);

			using (var context = UIGraphics.GetCurrentContext()) {
				context.SaveState ();
				context.SetAlpha (0.3f);
				context.SetFillColor (UIColor.Black.CGColor);
				context.FillRect (rect);
				//context.SetShadowWithColor (new SizeF (0, -1), 0.5f, UIColor.White.CGColor);
				context.SetAlpha (1f);
				var i = 0;
				UIColor.White.SetColor();
				foreach (var d in Enum.GetNames(typeof(DayOfWeek))) {

					DrawString (d.Substring (0, 1), new RectangleF (i * 46, 44 - 12, 45, 15), font,
						UILineBreakMode.WordWrap, UITextAlignment.Center);
					i++;
				}
				context.RestoreState ();
			}
		}
	}

	public class WeekGridView : UIView
	{
		private CalendarWeekView _calendarWeekView;

		public DateTime CurrentDate {get;set;}
		protected readonly List<CalendarWeekDayView> _dayTiles = new List<CalendarWeekDayView>();
		public int Lines { get; set; }
		public CalendarWeekDayView SelectedDayView {get;set;}
		public int weekdayOfFirst;
		public IList<DateTime> Marks { get; set; }

		public List<CalendarWeekDayView> DayViews
		{
			get{return _dayTiles;}
		}

		public WeekGridView(CalendarWeekView calendarWeekView, DateTime date)
		{
			_calendarWeekView = calendarWeekView;
			CurrentDate = date;
		}

		public void Update()
		{
			foreach (var v in _dayTiles)
			{
				updateDayView(v);
			}

			this.SetNeedsDisplay();
		}

//		public void Reload()
//		{
//			foreach (var v in _dayTiles)
//			{
//				if (!v.Date.IsInCurrentWeek (CurrentDate)) {
//					continue;
//				}
//			
//			}
//			this.SetNeedsDisplay()

		public void updateDayView(CalendarWeekDayView dayView){
			dayView.Available = _calendarWeekView.IsDateAvailable == null ? true : _calendarWeekView.IsDateAvailable(dayView.Date);
		}

		public void RefreshGrid()
		{
			foreach (var dayView in DayViews)
			{
				double totalProtein = 0;
				ProCalManager.Instance.TotalProteinByDate.TryGetValue (dayView.Date, out totalProtein);
				dayView.TotalProteinGrams = totalProtein;
				dayView.Text = dayView.TotalProteinGrams.ToString ();
				dayView.ProteinAward = ProteinCalculator.CalculateAward (dayView.TotalProteinGrams);
			}
		}

		public void RefreshDayView()
		{
			double totalProtein = 0;
			ProCalManager.Instance.TotalProteinByDate.TryGetValue (SelectedDayView.Date, out totalProtein);
			SelectedDayView.TotalProteinGrams = totalProtein;
			SelectedDayView.Text = SelectedDayView.TotalProteinGrams.ToString ();
			SelectedDayView.ProteinAward = ProteinCalculator.CalculateAward (SelectedDayView.TotalProteinGrams);
		}

		public void BuildGrid()
		{
			// 7 day range
			// S M T W T F S
			// get current date - need to get the week
			DateTime startOfWeek = CurrentDate.StartOfWeek (DayOfWeek.Sunday);

			for (int i = 0; i < 7; i++)
			{
				var viewDay = startOfWeek.AddDays(i);
				var dayView = new CalendarWeekDayView
				{
					BackgroundColor = UIColor.Clear,
					Frame = new RectangleF(i * 46 - 1, 0, 47, 44),
					Today = (viewDay == DateTime.Today),
					Active =  viewDay.Date <= DateTime.Today,
					Tag = viewDay.Day,
					Selected = (i == ((int)CurrentDate.DayOfWeek))
				};
				dayView.Date = viewDay;

				double totalProtein = 0;
				ProCalManager.Instance.TotalProteinByDate.TryGetValue (viewDay.Date, out totalProtein);
				dayView.TotalProteinGrams = totalProtein;
				dayView.Text = dayView.TotalProteinGrams.ToString ();
				dayView.ProteinAward = ProteinCalculator.CalculateAward (dayView.TotalProteinGrams);
				updateDayView(dayView);

				if (dayView.Selected)
					SelectedDayView = dayView;

				AddSubview(dayView);
				_dayTiles.Add(dayView);
				//_currentMonthDayTiles.Add (dayView);
			}

			Frame = new RectangleF(Frame.Location, new SizeF(Frame.Width, 44));
			if (SelectedDayView!=null)
				this.BringSubviewToFront(SelectedDayView);
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);

			if (_calendarWeekView.IsLocked) {
				return;
			}

			if (SelectDayView ((UITouch)touches.AnyObject) && _calendarWeekView.OnDateSelected != null)
			{
				var date = new DateTime (CurrentDate.Year, CurrentDate.Month, SelectedDayView.Tag);
				_calendarWeekView.OnDateSelected (date);
				_calendarWeekView.UpdateDateLabel (date);
			}
		}
	
		private bool SelectDayView(UITouch touch){
			var p = touch.LocationInView(this);

			int index = ((int)p.Y / 44) * 7 + ((int)p.X / 46);
			if(index<0 || index >= _dayTiles.Count) return false;

			var newSelectedDayView = _dayTiles[index];
			if (newSelectedDayView == SelectedDayView) 
				return false;

			if (SelectedDayView != null)
			{
				SelectedDayView.Selected = false;
			}

			_dayTiles.ForEach(d => {
				if(d.Selected)
				{
					d.Selected = false;
				}
			});

			newSelectedDayView.Alpha = 0;
			newSelectedDayView.Selected = true;
			UIView.Animate (0.2, 0, UIViewAnimationOptions.CurveLinear, () => {
				newSelectedDayView.Alpha = 1;
			}, null);

			SelectedDayView = newSelectedDayView;
			_calendarWeekView.CurrentDate = SelectedDayView.Date;
			CurrentDate = SelectedDayView.Date;
			return true;
		}

		public void DeselectDayView(){
			if (SelectedDayView==null) return;
			SelectedDayView.Selected= false;
			SelectedDayView = null;
			SetNeedsDisplay();
		}
	}

	public class CalendarWeekDayView : UIView
	{
		private BaseAnimationDelegate _animDelegate;
		private CABasicAnimation _animation;
		private string _text;
		private double _totalGramsOfProtein;
		private ProteinAward _award;
		private bool _active, _today, _selected, _available;
		public double TotalProteinGrams{get{ return _totalGramsOfProtein; }set{_totalGramsOfProtein = value;SetNeedsDisplay ();}}
		public bool Available {get {return _available; } set {_available = value; SetNeedsDisplay(); }}
		public string Text {get { return _text; } set { _text = value; SetNeedsDisplay(); } }
		public bool Active {get { return _active; } set { _active = value; SetNeedsDisplay();  } }
		public bool Today {get { return _today; } set { _today = value; SetNeedsDisplay(); } }
		public bool Selected {get { return _selected; } set { _selected = value; SetNeedsDisplay(); } }
		public ProteinAward ProteinAward {
			get{ return _award; }
			set {
				_award = value;
				SetNeedsDisplay ();
			}
		}
		public DateTime Date {get;set;}

		public void Glow()
		{
			var glowView = new UIImageView (UIImage.FromBundle ("gold-off"));
			glowView.Center = this.Center;
			Superview.InsertSubviewAbove (glowView, this);

			glowView.Alpha= 0;
			glowView.Layer.ShadowColor = UIColor.White.CGColor;;
			glowView.Layer.ShadowOffset = SizeF.Empty;
			glowView.Layer.ShadowRadius = 15;
			glowView.Layer.ShadowOpacity = 1.0f;

			_animation = CABasicAnimation.FromKeyPath ("opacity");
			_animDelegate = new BaseAnimationDelegate (glowView, _animation);
			_animDelegate.AnimationCompleted += (o, s) => {
				glowView.RemoveFromSuperview();
			};
			_animation.Duration = 0.7;
			_animation.From = NSNumber.FromFloat (0.1f);
			_animation.To = NSNumber.FromFloat(0.6f);
			_animation.TimingFunction = CAMediaTimingFunction.FromName (CAMediaTimingFunction.EaseInEaseOut);
			_animation.AutoReverses = true;
			_animation.RepeatCount = 1;
			_animation.RemovedOnCompletion = true;
			_animation.Delegate = _animDelegate;
			glowView.Layer.AddAnimation(_animation, "glow");
		}

		public override void Draw(RectangleF rect)
		{
			UIImage img = null;
			UIColor color = UIColor.White;

			if (!Active || !Available)
			{
				// future days
				color = UIColor.FromRGBA(0.576f, 0.608f, 0.647f, 1f);
			} 
			else if (Selected)
			{
				img = GetImage (true);
			}
			else
			{
				img = GetImage (false);
			}

			if(img != null)
			{
				img.Draw(new PointF(0, 0));
			}

			using (var context = UIGraphics.GetCurrentContext())
			{
				color.SetColor();
				RectangleF frame = rect;
				frame.Y = Text.Length <= 3 ? 11 : 14;
				frame.Height = 22;
				DrawString(Text, frame, Text.Length <= 3 ? UIFont.SystemFontOfSize(17) : UIFont.SystemFontOfSize(13), UILineBreakMode.WordWrap, UITextAlignment.Center);
			}

		}

		private UIImage GetImage(bool isSelected)
		{
			switch (ProteinAward) {
			case ProteinAward.None:
				return isSelected ? UIImage.FromBundle("today") : null;
			case ProteinAward.Bronze:
				return isSelected ? UIImage.FromBundle ("bronze-on") : UIImage.FromBundle ("bronze-off");
			case ProteinAward.Silver:
				return isSelected ? UIImage.FromBundle ("silver-on") : UIImage.FromBundle ("silver-off");
			case ProteinAward.Gold:
				return isSelected ? UIImage.FromBundle ("gold-on") : UIImage.FromBundle ("gold-off");
			}
			return null;
		}
	}
}

