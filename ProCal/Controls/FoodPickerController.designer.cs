// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("FoodPickerController")]
	partial class FoodPickerController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton addFoodButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView backgroundImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton closeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView foodTableView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISearchBar searchBar { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backgroundImageView != null) {
				backgroundImageView.Dispose ();
				backgroundImageView = null;
			}

			if (addFoodButton != null) {
				addFoodButton.Dispose ();
				addFoodButton = null;
			}

			if (closeButton != null) {
				closeButton.Dispose ();
				closeButton = null;
			}

			if (foodTableView != null) {
				foodTableView.Dispose ();
				foodTableView = null;
			}

			if (searchBar != null) {
				searchBar.Dispose ();
				searchBar = null;
			}
		}
	}
}
