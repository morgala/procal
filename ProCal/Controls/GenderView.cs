using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Back2Front.Utilities;

namespace ProCal
{
	public partial class GenderView : UIView
	{
		public event EventHandler<EventArgs> GenderSelected;

		public GenderView ()
		{
		}

		public GenderView (IntPtr handle) : base(handle)
		{
		}

		public User User
		{
			get{ return ProCalManager.Instance.User; }
		}

		public void Initialise()
		{
			maleButton.TouchUpInside += (sender, e) => SelectGender(Sex.Male);
			femaleButton.TouchUpInside += (sender, e) => SelectGender(Sex.Female);
		}

		private void SelectGender(Sex sex)
		{
			User.Sex = sex;
			if (GenderSelected != null) {
				GenderSelected (this, null);
			}
		}

		public static GenderView GetView()
		{
			var view =  NibLoader.LoadUIObject<GenderView>("GenderView");
			view.Initialise();
			return view;
		}
	}
}

