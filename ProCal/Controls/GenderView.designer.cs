// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("GenderView")]
	partial class GenderView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton femaleButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton maleButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (femaleButton != null) {
				femaleButton.Dispose ();
				femaleButton = null;
			}

			if (maleButton != null) {
				maleButton.Dispose ();
				maleButton = null;
			}
		}
	}
}
