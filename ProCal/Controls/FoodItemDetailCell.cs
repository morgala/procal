using System;
using System.Drawing;
using System.Globalization;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ProCal
{
	public partial class FoodItemDetailCell : UITableViewCell
	{
		private Food _food;
		private UIView _backgroundModalView;
		private AddFoodView _addFoodView;

		public static readonly UINib Nib = UINib.FromName ("FoodItemDetailCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("FoodItemDetailCell");

		public FoodItemDetailCell (IntPtr handle) : base (handle)
		{
		}

		public Food Food
		{
			get{ return _food; }
			set {
				_food = value;

				editButton.Hidden = !_food.IsCustom;
			}	 
		}

		public static FoodItemDetailCell Create ()
		{
			return (FoodItemDetailCell)Nib.Instantiate (null, null) [0];
		}

		public void Initialise()
		{

			if (ProCalManager.Instance.CurrentSelectedDate < DateTime.Today) {
				datePicker.SetDate (ProCalManager.Instance.CurrentSelectedDate, true);
			}

			editButton.TouchUpInside += (sender, e) => 
			{
				_addFoodView = AddFoodView.GetView ();
				_addFoodView.Food = _food;
				_addFoodView.Frame = new RectangleF((UIScreen.MainScreen.Bounds.Width - _addFoodView.Frame.Width)/2 , (UIScreen.MainScreen.Bounds.Height - _addFoodView.Frame.Height)/2, _addFoodView.Frame.Width, _addFoodView.Frame.Height);
				_backgroundModalView = new UIView(GetTableView().Bounds);
				_backgroundModalView.BackgroundColor = UIColor.Black.ColorWithAlpha (0.6f);
				GetTableView().UserInteractionEnabled = false;
				UIApplication.SharedApplication.KeyWindow.AddSubview(_backgroundModalView);
				UIApplication.SharedApplication.KeyWindow.AddSubview((UIView)_addFoodView);
				_addFoodView.Animate();
				_addFoodView.Close += (s, ev) => {
					_backgroundModalView.RemoveFromSuperview ();
					((UIView)_addFoodView).RemoveFromSuperview ();
					_backgroundModalView = null;
					_addFoodView = null;
					GetTableView().UserInteractionEnabled = true;

					// reload table.
					if(ev.HasNewFoodBeenCreated)
					{
						GetTableView().ReloadData();
					}
				};
			};

			addButton.TouchUpInside += (sender, e) => 
			{
				double servings = double.Parse(servingsLabel.Text, CultureInfo.InvariantCulture);
				double totalAmount = servings * Food.ProteinGrams;
				DateTime startDate = DateTime.Parse(datePicker.Date.ToString());
				FoodItem foodItem = new FoodItem()
				{
					Food = Food,
					Servings = servings,
					StartDate = (startDate == DateTime.Today ? DateTime.Now : startDate),
					IsHistoric = datePicker.Date < DateTime.Today
				};

				var message = string.Format("{0} servings of {1} ({2}g of protein) has been added to your log",
				                            servingsLabel.Text, Food.Name, totalAmount);
				var alert = new UIAlertView("Food Logged", message, null, "OK",null);
				alert.Show();

				ProCalManager.Instance.AddFoodItem(foodItem);
				ProCalManager.Instance.UpdateFavouriteCount(Food);

			};
			stepper.ValueChanged += (sender, e) => 
			{
				servingsLabel.Text = stepper.Value.ToString();
			};
		}



		private UITableView GetTableView()
		{
			var view = Superview;
			while (!(view is UITableView)) {
				view = view.Superview;
			}
			return view as UITableView;
		}
	}
}

