using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ProCal
{
	public partial class TrackerTableCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("TrackerTableCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("TrackerTableCell");

		public TrackerTableCell (IntPtr handle) : base (handle)
		{
		}

		public static TrackerTableCell Create ()
		{
			var cell = (TrackerTableCell)Nib.Instantiate (null, null) [0];
			cell.TextLabel.TextColor = UIColor.White;
			cell.DetailTextLabel.TextColor = UIColor.White;
			return cell;
		}
	}
}

