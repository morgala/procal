// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("WeightView")]
	partial class WeightView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton changeWeightButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton okButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField weightTextBox { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (changeWeightButton != null) {
				changeWeightButton.Dispose ();
				changeWeightButton = null;
			}

			if (weightTextBox != null) {
				weightTextBox.Dispose ();
				weightTextBox = null;
			}

			if (okButton != null) {
				okButton.Dispose ();
				okButton = null;
			}
		}
	}
}
