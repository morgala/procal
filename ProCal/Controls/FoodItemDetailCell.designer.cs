// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("FoodItemDetailCell")]
	partial class FoodItemDetailCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton addButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIDatePicker datePicker { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton editButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel servingsLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIStepper stepper { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (addButton != null) {
				addButton.Dispose ();
				addButton = null;
			}

			if (datePicker != null) {
				datePicker.Dispose ();
				datePicker = null;
			}

			if (servingsLabel != null) {
				servingsLabel.Dispose ();
				servingsLabel = null;
			}

			if (stepper != null) {
				stepper.Dispose ();
				stepper = null;
			}

			if (editButton != null) {
				editButton.Dispose ();
				editButton = null;
			}
		}
	}
}
