// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("AddFoodView")]
	partial class AddFoodView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton addButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton closeButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField foodNameTextField { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField proteinTextField { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (addButton != null) {
				addButton.Dispose ();
				addButton = null;
			}

			if (closeButton != null) {
				closeButton.Dispose ();
				closeButton = null;
			}

			if (foodNameTextField != null) {
				foodNameTextField.Dispose ();
				foodNameTextField = null;
			}

			if (proteinTextField != null) {
				proteinTextField.Dispose ();
				proteinTextField = null;
			}
		}
	}
}
