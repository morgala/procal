// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("DailyIntakeView")]
	partial class DailyIntakeView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton okButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField weightTextField { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel weightUnitLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (weightTextField != null) {
				weightTextField.Dispose ();
				weightTextField = null;
			}

			if (weightUnitLabel != null) {
				weightUnitLabel.Dispose ();
				weightUnitLabel = null;
			}

			if (okButton != null) {
				okButton.Dispose ();
				okButton = null;
			}
		}
	}
}
