using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Back2Front.GUI;
using Back2Front.Utilities;

namespace ProCal
{
	public partial class WeightView : UIView
	{
		private UIView _dismiss;
		public event EventHandler<EventArgs> WeightSelected;

		public WeightView ()
		{
		}

		public WeightView (IntPtr handle) : base(handle)
		{
		}

		public User User
		{
			get{ return ProCalManager.Instance.User; }
		}

		public override UIView InputAccessoryView 
		{
			get 
			{
				if (_dismiss == null)
				{
					_dismiss = new UIView(new RectangleF(0,0,320,44));
					_dismiss.BackgroundColor = UIColor.White.ColorWithAlpha (0.95f);
					UIButton dismissBtn = UIButton.FromType (UIButtonType.System);
					dismissBtn.SetTitleColor (UIColor.FromRGB (80, 169, 237), UIControlState.Normal);
					dismissBtn.SetTitleColor (UIColor.White, UIControlState.Highlighted);
					dismissBtn.TintColor = UIColor.White;
					dismissBtn.Frame = new RectangleF (240, 0, 58, 44);
					dismissBtn.SetTitle ("Done", UIControlState.Normal);
					dismissBtn.TouchUpInside  += delegate {
						EndEditing(true);
					};
					_dismiss.AddSubview(dismissBtn);
				}
				return _dismiss;
			}
		}


		public void Initialise()
		{
			weightTextBox.ShouldReturn = delegate(UITextField textField) {
				weightTextBox.ResignFirstResponder();
				User.Weight = Double.Parse( weightTextBox.Text);
				return false;
			};

			okButton.TouchUpInside += (sender, e) => {
				if(weightTextBox.Text == "???")
				{
					weightTextBox.Wobble();
					return;
				}

				if(WeightSelected != null)
				{
					WeightSelected(this, null);
				}
			};

			changeWeightButton.TouchUpInside += (sender, e) => {
				if(weightTextBox.Text == "???" || string.IsNullOrEmpty(weightTextBox.Text))
				{
					weightTextBox.Wobble();
					return;
				}
				double weight = Double.Parse(weightTextBox.Text);
				if(User.IsMetric)
				{
					User.Weight = ProteinCalculator.ConvertToLbs(weight);
					changeWeightButton.SetTitle("lbs", UIControlState.Normal);
				}
				else
				{
					User.Weight = ProteinCalculator.ConvertToKg(weight);
					changeWeightButton.SetTitle("kg", UIControlState.Normal);
				}
				weightTextBox.Text = Math.Round(User.Weight).ToString();
				User.IsMetric = !User.IsMetric;
			};
		}

		public static WeightView GetView()
		{
			var view =  NibLoader.LoadUIObject<WeightView>("WeightView");
			view.Initialise();
			return view;
		}
	}
}

