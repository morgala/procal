using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Back2Front.Utilities;

namespace ProCal
{
	public partial class DailyIntakeView : UIView
	{
		public event EventHandler<EventArgs> Close;

		public DailyIntakeView ()
		{
		}

		public DailyIntakeView (IntPtr handle) : base(handle)
		{
		}

		public User User
		{
			get{ return ProCalManager.Instance.User; }
		}

		public void Finished()
		{
			if (Close != null) {
				Close (this, null);
			}
		}

		public void Initialise()
		{
			weightTextField.ShouldReturn = delegate(UITextField textField) {
				weightTextField.ResignFirstResponder();
				User.DailyProteinIntakeLimit = Double.Parse( weightTextField.Text);
				return false;
			};

			okButton.TouchUpInside += (sender, e) => Finished();
		}

		public void Update()
		{
			ProteinCalculator.CalculateDailyIntake (User);
			weightTextField.Text = User.DailyProteinIntakeLimit.ToString ();
		}

		public static DailyIntakeView GetView()
		{
			var view =  NibLoader.LoadUIObject<DailyIntakeView>("DailyIntakeView");
			view.Initialise();
			return view;
		}
	}
}

