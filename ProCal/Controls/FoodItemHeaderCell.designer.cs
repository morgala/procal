// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("FoodItemHeaderCell")]
	partial class FoodItemHeaderCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel foodLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel proteinGramsLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (foodLabel != null) {
				foodLabel.Dispose ();
				foodLabel = null;
			}

			if (proteinGramsLabel != null) {
				proteinGramsLabel.Dispose ();
				proteinGramsLabel = null;
			}
		}
	}
}
