using System;
using System.Drawing;
using MonoTouch.AudioToolbox;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Back2Front.Utilities;
using Back2Front.GUI;

namespace ProCal
{
	public partial class AddFoodView : UIView
	{
		private Food _food;
		public event EventHandler<NewFoodEventArgs> Close;

		public AddFoodView ()
		{
		}

		public AddFoodView (IntPtr handle) : base(handle)
		{
		}

		public Food Food
		{
			get{ return _food; }
			set{ 
				_food = value; 
				foodNameTextField.Text = _food.Name;
				proteinTextField.Text = _food.ProteinGrams.ToString();
			}
		}

		public void Initialise()
		{
			foodNameTextField.Delegate = new CatchEnterDelegate ();
			UIStringAttributes attributes = new UIStringAttributes ();
			attributes.ForegroundColor = UIColor.Black.ColorWithAlpha(0.7f);
			foodNameTextField.AttributedPlaceholder = new NSAttributedString (foodNameTextField.Placeholder, attributes);
			proteinTextField.AttributedPlaceholder = new NSAttributedString (proteinTextField.Placeholder, attributes);


			closeButton.TouchUpInside += (sender, e) => CloseView(false);
			addButton.TouchUpInside += (sender, e) => {
				try
				{
					if(string.IsNullOrEmpty(foodNameTextField.Text))
					{
						AnimateTextField(foodNameTextField);
						return;
					}
					if(string.IsNullOrEmpty(proteinTextField.Text))
					{
						AnimateTextField(proteinTextField);
						return;
					}

					if(_food != null)
					{
						_food.Name = foodNameTextField.Text;
						_food.ProteinGrams = Double.Parse( proteinTextField.Text);
						ProCalManager.Instance.UpdateFood(_food);
					}
					else
					{
						var food = new Food();
						food.Name = foodNameTextField.Text;
						food.ProteinGrams = Double.Parse( proteinTextField.Text);
						food.IsCustom = true;
						ProCalManager.Instance.AddFood(food);
					}

					CloseView(true);
				}
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
					var alertView = new UIAlertView("Error", "There was a problem adding the food data", null, "OK", null);
					alertView.Show();
				}
			}; 
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			EndEditing(true);
		}

		public void Animate()
		{
			Transform = CGAffineTransform.MakeScale(0.8f, 0.8f);
			UIView.Animate(0.3, () =>
			               {
				Transform = CGAffineTransform.MakeIdentity();
			});
		}

		public static AddFoodView GetView()
		{
			AddFoodView view =  NibLoader.LoadUIObject<AddFoodView>("AddFoodView");
			view.Initialise();
			view.Layer.ShadowOpacity = 0.2f;
			view.Layer.ShadowOffset = new SizeF(6,6);
			view.Layer.ShouldRasterize = true;
			view.Layer.RasterizationScale =  UIScreen.MainScreen.Scale;
			return view;
		}

		private void CloseView(bool newItemAdded)
		{
			if(Close != null)
			{
				Close(this, new NewFoodEventArgs{HasNewFoodBeenCreated = newItemAdded});
			}
		}

		private static void AnimateTextField(UITextField textField)
		{
			if(textField.IsFirstResponder)
			{
				textField.ResignFirstResponder();
			}
			textField.Wobble ();
			SystemSound.Vibrate.PlaySystemSound();
		}

		public class CatchEnterDelegate : UITextFieldDelegate
		{
			public override bool ShouldReturn(UITextField textField)
			{
				textField.ResignFirstResponder();
				return true;
			}
		}
	}

	public class NewFoodEventArgs : EventArgs
	{
		public bool HasNewFoodBeenCreated{get;set;}
	}
}

