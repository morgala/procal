using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ProCal
{
	public partial class FoodItemHeaderCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("FoodItemHeaderCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("FoodItemHeaderCell");

		public FoodItemHeaderCell (IntPtr handle) : base (handle)
		{
		}

		public UILabel FoodLabel{
			get{ return foodLabel; }
		}

		public UILabel ProteinLabel{
			get{ return proteinGramsLabel; }
		}

		public bool IsSelected{ get; set; }

		public Food Food
		{
			set{
				foodLabel.Text = value.Name;
				proteinGramsLabel.Text = string.Format ("{0}g", value.ProteinGrams);
			}
		}

		public static FoodItemHeaderCell Create ()
		{
			return (FoodItemHeaderCell)Nib.Instantiate (null, null) [0];
		}
	}
}

