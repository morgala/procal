using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Back2Front.Utilities;

namespace ProCal
{
	public partial class ActivityLevelView : UIView
	{
		public event EventHandler<EventArgs> ActivityLevelSelected;

		public ActivityLevelView ()
		{
		}

		public ActivityLevelView (IntPtr handle) : base(handle)
		{
		}

		public User User
		{
			get{ return ProCalManager.Instance.User; }
		}

		public void Initialise()
		{
			sedButton.TouchUpInside += (sender, e) => {
				User.ActivityLevel = ActivityLevel.Sedentary;
				LevelSelected();
			};
			lightButton.TouchUpInside += (sender, e) => {
				User.ActivityLevel = ActivityLevel.Light;
				LevelSelected();
			};
			modButton.TouchUpInside += (sender, e) => {
				User.ActivityLevel = ActivityLevel.Moderate;
				LevelSelected();
			};
			veryActiveButton.TouchUpInside += (sender, e) => {
				User.ActivityLevel = ActivityLevel.VeryActive;
				LevelSelected();
			};
			heavyButton.TouchUpInside += (sender, e) => {
				User.ActivityLevel = ActivityLevel.Heavy;
				LevelSelected();
			};
		}

		public void LevelSelected()
		{
			if (ActivityLevelSelected != null) {
				ActivityLevelSelected (this, null);
			}
		}

		public static ActivityLevelView GetView()
		{
			var view =  NibLoader.LoadUIObject<ActivityLevelView>("ActivityLevelView");
			view.Initialise();
			return view;
		}
	}
}

