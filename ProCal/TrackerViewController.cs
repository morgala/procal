using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;

using Back2Front.Animation;
using Back2Front.Utilities;

namespace ProCal
{
	public partial class TrackerViewController : UIViewController
	{
		public CalendarWeekView _weekView;
		private FoodPickerController _foodPickerController;
		private FoodHistoryTableSource _tableSource;
		private bool _hasRefreshed;

		public TrackerViewController (IntPtr handle) : base (handle)
		{
		}

		public CalendarWeekView CalendarWeekView
		{
			get{ return _weekView;}
		}
		public User User { get; private set; }

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}

		public bool NeedsRefresh
		{
			get{

				if (DateTime.Today > ProCalManager.Instance.ResignTimeStamp.Date)
				{
					if (_hasRefreshed)
					{
						return false;
					}else
					{
						_hasRefreshed = true;
						return true;
					}
				}
				return false;
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			bronzeProgressBar.TrackTintColor = UIColor.Black;
			silverProgressBar.TrackTintColor = UIColor.Black;
			goldProgressBar.TrackTintColor = UIColor.Black;

			/******* FOREGROUND OBSERVER *********/
			NSNotificationCenter.DefaultCenter.AddObserver (UIApplication.WillEnterForegroundNotification, (n) => 
			{
				try
				{
					if(DateTime.Today > ProCalManager.Instance.ResignTimeStamp.Date)
					{
						_weekView.MoveToToday();
						_weekView.RefreshWeek();
						_weekView.UpdateDateLabel(DateTime.Now);
					}
				}
				catch(Exception e)
				{
					Console.WriteLine(e.Message);
				}
			});
			/*************************************/

			_weekView = new CalendarWeekView(new RectangleF(0, 20, 320, 100));
			_weekView.OnDateSelected = (date) => 
			{
				ProCalManager.Instance.CurrentSelectedDate = date;
				List<FoodItem> foodItems = null;
				ProCalManager.Instance.FoodItems.TryGetValue(date, out foodItems);
				if(foodItems != null)
				{
					var list = foodItems.OrderByDescending (l => l.StartDate);
					_tableSource.Data = list.ToList();
					foodTable.ReloadData();
					double dailyProteinTotal = 0;
					ProCalManager.Instance.TotalProteinByDate.TryGetValue(date, out dailyProteinTotal);
					CalculateDailyProteinProgress(dailyProteinTotal);
					if(date == DateTime.Today)
					{
						// if reached todays total then cancel all notifications
						double percentTotal = dailyProteinTotal / User.DailyProteinIntakeLimit;
						if(percentTotal >= 1)
						{
							UIApplication.SharedApplication.CancelAllLocalNotifications();
						}
					}
				}
				else
				{
					_tableSource.Data = new List<FoodItem>();
					foodTable.ReloadData();
					CalculateDailyProteinProgress(0);
				}
			};
			_weekView.IsDateAvailable = (date)=>{
				return (date <= DateTime.Today);
			};
			View.AddSubview(_weekView);

			todayButton.TouchUpInside += (sender, e) => 
			{
				_weekView.MoveToToday();
			};
			addFoodButton.TouchUpInside += AddFoodButtonOnTouch;

			_tableSource = new FoodHistoryTableSource(null, this);
			foodTable.Source = _tableSource;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			try{

				User = ProCalManager.Instance.User;
				backgroundImageView.Image = User.Sex == Sex.Male ? UIImage.FromFile ("bg.png") : UIImage.FromFile ("bg-female.png");

				// check if the 
				if(ProCalManager.Instance.UpdateCalendar || !_weekView.CurrentDate.IsInCurrentWeek())
				{
					_weekView.RefreshWeek();
					ProCalManager.Instance.UpdateCalendar = false;
				}
			}
			catch(Exception e) {
				Console.WriteLine (e.Message);
				//Logger.Instance.Log (e);
			}
		}

		void AddFoodButtonOnTouch (object sender, EventArgs e)
		{
			_foodPickerController = new FoodPickerController();
			_foodPickerController.Close += (s, ev) => {
				_foodPickerController.DismissViewController (true, null);
				foodTable.ReloadData();
				ProCalManager.Instance.AllFoods.Sort();
			};
			PresentViewController (_foodPickerController, true, null);
		}
	
		public void CalculateDailyProteinProgress(double todaysTotalProtein)
		{
			// reset to zero
			bronzeProgressBar.SetProgress (0, true);
			silverProgressBar.SetProgress (0, true);
			goldProgressBar.SetProgress (0, true);

			dailyTotalLabel.Text = string.Format("{0}/{1}g", todaysTotalProtein, User.DailyProteinIntakeLimit);
			ProteinAward award = ProteinCalculator.CalculateAward (todaysTotalProtein);
			switch (award) {
			case ProteinAward.None:
				bronzeProgressBar.SetProgress (0, true);
				silverProgressBar.SetProgress (0, true);
				goldProgressBar.SetProgress (0, true);
				break;
			case ProteinAward.Bronze:
				double bronzeProgress = todaysTotalProtein / ProteinCalculator.BronzeUpperLimit;
				bronzeProgressBar.SetProgress ((float)bronzeProgress, false);
				break;
			case ProteinAward.Silver:
				double silverLimit = (ProteinCalculator.GoldLowerLimit - ProteinCalculator.BronzeUpperLimit) - (ProteinCalculator.GoldLowerLimit - todaysTotalProtein);
				double silverProgress = silverLimit / (ProteinCalculator.GoldLowerLimit - ProteinCalculator.BronzeUpperLimit);
				bronzeProgressBar.SetProgress (1, true);
				silverProgressBar.SetProgress ((float)silverProgress, true);
				break;
			case ProteinAward.Gold:
				double goldLimit = (User.DailyProteinIntakeLimit - ProteinCalculator.GoldLowerLimit) - (User.DailyProteinIntakeLimit - todaysTotalProtein);
				double goldProgress = goldLimit / (User.DailyProteinIntakeLimit - ProteinCalculator.GoldLowerLimit);

				bronzeProgressBar.SetProgress (1, true);
				silverProgressBar.SetProgress (1, true);
				if (goldProgress >= 1) {
					goldProgressBar.SetProgress (1, true);
				} else {
					goldProgressBar.SetProgress ((float)goldProgress, true);
				}
				break;
			}
	
		}

		private void RunAnim()
		{
			var vertAnim = new VerticalBounceAnimation ("animVert", containerView)
			{
				FromValue = new NSNumber(containerView.Center.Y),
				ToValue = new NSNumber(containerView.Frame.Y),
				NumberOfBounces = 4,
				Duration = 1f,
				ShouldOvershoot = true
			};
			vertAnim.Initialise ();
			containerView.Layer.AddAnimation (vertAnim.Animation, vertAnim.Key);
			containerView.Layer.SetValueForKeyPath (vertAnim.ToValue, new NSString (vertAnim.KeyPath));
		}

		public class FoodHistoryTableSource : UITableViewSource
		{
			private string _cellIdentifier = "cellID";
			private TrackerViewController _calendarVC;

			public FoodHistoryTableSource (List<FoodItem> data, TrackerViewController vc)
			{
				_calendarVC = vc;
				Data = data; 
			}

			public List<FoodItem> Data{
				get;set;
			}

			public override int RowsInSection (UITableView tableview, int section)
			{
				return Data == null ? 0 : Data.Count;
			}

			public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
			{
				TrackerTableCell cell = tableView.DequeueReusableCell (_cellIdentifier) as TrackerTableCell;
				if(cell == null)
				{
					cell = TrackerTableCell.Create ();
				}
				var food = Data[indexPath.Row];
				cell.TextLabel.Text = string.Format ("{0} - {1}g", food.Food.Name, food.TotalProteinGrams);
				if (food.IsHistoric)
				{
					cell.DetailTextLabel.Text = food.StartDate.ToString ("dd MMM yyyy");
				}
				else
				{
					// TODO: check format of time, ensure 24hour.
					cell.DetailTextLabel.Text = food.StartDate.ToLongTimeString ();
				}

				return cell;
			}

			public override void CommitEditingStyle (UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
			{
				try
				{
					if (editingStyle == UITableViewCellEditingStyle.Delete)
					{
						var food = Data [indexPath.Row];
						Data.RemoveAt(indexPath.Row);
						tableView.DeleteRows(new [] { indexPath }, UITableViewRowAnimation.Fade);
						ProCalManager.Instance.DeleteFoodHistory(food);
						_calendarVC.CalendarWeekView.RefreshWeek();
						_calendarVC.CalculateDailyProteinProgress(Data.Sum(f => f.TotalProteinGrams));
					}
				}
				catch(Exception e){
					Console.WriteLine (e.Message);
					//Logger.Instance.Log (e);
				}
			}
		}

		public class SwipeRecogniserDelegate : UIGestureRecognizerDelegate
		{
			public override bool ShouldReceiveTouch (UIGestureRecognizer recognizer, UITouch touch)
			{
				return true;
			}
		}
	}
}
	