using System;
using MonoTouch.UIKit;

namespace ProCal
{
	public enum ProteinAward
	{
		None,
		Gold,
		Silver,
		Bronze
	}

	public static class ProteinAwardColors
	{
		private static UIColor _bronzeColor = UIColor.FromRGB (180,117,11);
		private static UIColor _silverColor = UIColor.FromRGB (153,153,153); 
		private static UIColor _goldColor = UIColor.FromRGB (255,180,0);

		public static UIColor BronzeColor
		{
			get{return _bronzeColor;}
		}
		public static UIColor SilverColor
		{
			get{ return _silverColor; }
		}
		public static UIColor GoldColor
		{
			get{ return _goldColor; }
		}
	}
}

