using System;
using System.Collections.Generic;

namespace ProCal
{
	[Serializable]
	public class Food : IEquatable<Food>, IComparable<Food>
	{
		public Food ()
		{
		}

		public int Id{ get; set; }
		public string Name{ get; set;}
		public double ProteinGrams{ get; set;}
		public bool IsDummy{get;set;}
		public bool IsCustom{ get; set; }
		public int FavouriteCount{ get; set; }

		public bool Equals(Food other)
		{
			if(other == null)
				return false;

			return (Id == other.Id && Name == other.Name && ProteinGrams == other.ProteinGrams && IsCustom == other.IsCustom); 
		}

		public override bool Equals(Object obj)
		{
			if (obj == null) 
				return false;

			Food data = obj as Food;
			if (data == null)
				return false;
			else    
				return Equals(data);   
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode () ^ Name.GetHashCode () ^ ProteinGrams .GetHashCode() ^ IsCustom.GetHashCode();
		}

		public int CompareTo(Food other)
		{
			int primary = other.FavouriteCount.CompareTo (FavouriteCount);
			if (primary != 0) {
				return primary;
			}
			return Name.CompareTo (other.Name);
		}
	}
}

