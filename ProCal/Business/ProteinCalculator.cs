using System;
using System.Collections.Generic;

namespace ProCal
{
	public static class ProteinCalculator
	{
		public const double POUND_TO_KG = 2.2;
		private static double _bronzeUpperLimit;
		private static double _goldLowerLimit;
		private static Dictionary<Sex, Dictionary<ActivityLevel, double>> _calcFactors = new Dictionary<Sex, Dictionary<ActivityLevel, double>> (2);

		static ProteinCalculator()
		{
			var maleConstants = new Dictionary<ActivityLevel, double> (5);
			maleConstants [ActivityLevel.Sedentary] = 0.365;
			maleConstants [ActivityLevel.Light] = 0.45625;
			maleConstants [ActivityLevel.Moderate] = 0.56875;
			maleConstants [ActivityLevel.VeryActive] = 0.68125;
			maleConstants [ActivityLevel.Heavy] = 0.81875;

			var femaleConstants = new Dictionary<ActivityLevel, double> (5);
			femaleConstants [ActivityLevel.Sedentary] = 0.32;
			femaleConstants [ActivityLevel.Light] = 0.4;
			femaleConstants [ActivityLevel.Moderate] = 0.5;
			femaleConstants [ActivityLevel.VeryActive] = 0.6;
			femaleConstants [ActivityLevel.Heavy] = 0.72;
		
			_calcFactors [Sex.Male] = maleConstants;
			_calcFactors [Sex.Female] = femaleConstants;
		}


		public static double BronzeUpperLimit
		{
			get{ return _bronzeUpperLimit; }
		}
		public static double GoldLowerLimit
		{
			get{ return _goldLowerLimit; }
		}
	
		public static void CalculateDailyIntake(User user)
		{
			// calced in lbs
			var factors = _calcFactors [user.Sex];
			double weight = user.IsMetric ? user.Weight * 2.2 : user.Weight;
			double protein = Math.Round (weight * factors [user.ActivityLevel]);
			user.DailyProteinIntakeLimit = protein;
			CalculateDailyAwardRange (protein);
		}

		public static double ConvertToKg(double pounds)
		{
			return pounds / ProteinCalculator.POUND_TO_KG;
		}

		public static double ConvertToLbs(double kg)
		{
			return kg * ProteinCalculator.POUND_TO_KG;
		}

		public static void CalculateDailyAwardRange(double proteinIntake)
		{
			// bronze 50% 
			// silver 50% - 90%
			// gold > 90%
			_bronzeUpperLimit = proteinIntake * 0.5;
			_goldLowerLimit = proteinIntake * 0.98;
		}

		public static ProteinAward CalculateAward(double protein)
		{
			if (protein == 0)
			{
				return ProteinAward.None;
			}
			if (protein < _bronzeUpperLimit)
			{
				return ProteinAward.Bronze;
			}
			if (protein >= _bronzeUpperLimit && protein < _goldLowerLimit)
			{
				return ProteinAward.Silver;
			}
			return ProteinAward.Gold;
		}
	}
}

