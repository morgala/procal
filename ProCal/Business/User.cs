using System;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Back2Front.Utilities;

namespace ProCal
{
	[Serializable]
	public class User
	{
		private bool _notificationsEnabled;

		public User()
		{
		}

		public bool NotificationsEnabled
		{
			get{ return _notificationsEnabled;}
			set
			{
				_notificationsEnabled = value;
				UIApplication.SharedApplication.CancelAllLocalNotifications();
				if (NotificationsEnabled)
				{
					UILocalNotification notification = new UILocalNotification{
						FireDate  = DateTime.Now.AddHours(2),
						TimeZone = NSTimeZone.LocalTimeZone,
						RepeatInterval = NSCalendarUnit.Hour,
						AlertAction = "ProShake",
						AlertBody = "You need more protein!",
						SoundName = UILocalNotification.DefaultSoundName
					};

					UIApplication.SharedApplication.ScheduleLocalNotification (notification);
				}
			}
		}

		//ProteinCalculator.CalculateDailyAwardRange (DailyProteinIntakeLimit);
		public double Weight {get;set;}
		public bool IsMetric{get;set;}
		public Sex Sex{get;set;}
		public ActivityLevel ActivityLevel{ get; set; }
		public double DailyProteinIntakeLimit {get;set;}

		public double TodaysTotalProteinIntake
		{
			get{
				double proteinValue;
				if (ProCalManager.Instance.TotalProteinByDate.TryGetValue (DateTime.Today, out proteinValue)) {
					return proteinValue;
				}
				return 0;
			}
		}

		public double WeeklyProteinIntakeLimit
		{
			get{
				return DailyProteinIntakeLimit * 7;
			}
		}

		public double WeeklyProteinIntake
		{
			get{ 
				var startDate = DateTime.Today.StartOfWeek (DayOfWeek.Sunday);
				double totalWeeklyProtein = 0;
				for(int i = 0; i < 7; i++) {
					var date = startDate.AddDays(i);
					double protein;
					if (ProCalManager.Instance.TotalProteinByDate.TryGetValue (date, out protein)) {
						totalWeeklyProtein += protein;
					}
				}
				return totalWeeklyProtein;
			}
		}

		public static User CreateUser()
		{
			var user = new User () {
				Weight = 73,
				Sex = Sex.Male,
				ActivityLevel = ActivityLevel.Heavy,
				IsMetric = true
			};
			return user;
		}
	}

	public enum Sex
	{
		Male,
		Female
	}

	public enum ActivityLevel
	{
		Sedentary = 0,
		Light,
		Moderate,
		VeryActive,
		Heavy
	}
}

