using System;

namespace ProCal
{
	public class FoodItem : IComparable<FoodItem>, IEquatable<FoodItem>
	{
		public FoodItem ()
		{
		}

		public long Id{ get; set;}
		public Food Food{ get; set;}
		public double Servings{get;set;}
		public DateTime StartDate{get;set;}
		public bool IsHistoric{ get; set;}
		public double TotalProteinGrams
		{
			get{return Food.ProteinGrams * Servings;}
		}

		public int CompareTo(FoodItem other)
		{
			if(other == null)
			{
				return 1;
			}
			return StartDate.CompareTo (other.StartDate);
		}

		public bool Equals(FoodItem other)
		{
			if(other == null)
				return false;

			return Id == other.Id;
		}

		public override bool Equals(Object obj)
		{
			if (obj == null) 
				return false;

			FoodItem data = obj as FoodItem;
			if (data == null)
				return false;
			else    
				return Equals(data);   
		}

		public override int GetHashCode ()
		{
			return Id.GetHashCode ();
		}
	}
}

