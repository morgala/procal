using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using Mono.Data.Sqlite;

namespace ProCal
{
	public class ProCalManager
	{
		private static readonly object _syncLock = new object();
		private static ProCalManager _instance;
		private const string FOODHISTORY_DB = "foodDiary.sqlite";
		private const string SHIPPED_DB = "procal.sqlite";
		private readonly List<Food> _allFoods = new List<Food>();
		private readonly Dictionary<int, Food> _allFoodsDictionary = new Dictionary<int, Food>();
		private string _foodBankDbConnection;
		private string _foodDiaryDbConnection;
		private readonly string _sessionTokenPath;
		private bool _isInitialised;
		private readonly Dictionary<DateTime, List<FoodItem>> _foodItemsByDate = new Dictionary<DateTime, List<FoodItem>> ();
		private readonly Dictionary<DateTime, double> _totalSumProteinByDate = new Dictionary<DateTime, double> ();
		private readonly Dictionary<DateTime, ProteinAward> _proteinAwards = new Dictionary<DateTime, ProteinAward> ();
		private const string DATEFORMAT = "dd/MM/yyyy HH:mm:ss";
		private List<string> _dateFormats = new List<string> (5);

		public event EventHandler<EventArgs> FoodUpdated;

		private ProCalManager()
		{
			string path = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			string shippedDBPath = NSBundle.MainBundle.PathForResource("Data/procal", "sqlite");
			string diaryDBPath = NSBundle.MainBundle.PathForResource("Data/foodDiary", "sqlite");
			_foodBankDbConnection = Path.Combine (path, SHIPPED_DB);
			_foodDiaryDbConnection = Path.Combine (path, FOODHISTORY_DB);
			_sessionTokenPath = Path.Combine (path, "user.txt");

			_dateFormats.Add ("dd/MM/yyyy HH:mm:ss");
			_dateFormats.Add ("dd.MM.yyyy HH:mm:ss");
			_dateFormats.Add ("d.MM.yyyy HH:mm:ss");
			_dateFormats.Add ("dd.MM.yyyy. HH:mm:ss");
			_dateFormats.Add ("d.MM.yyyy. HH:mm:ss");

			// use this to test
			//DeleteUser ();

			var user = GetUserFromToken();
			if(user != null)
			{
				User = user;
			}
				

			/* Here we copy the shipped Db's and copy them to the users personal folder
			 * so that they can be edited. 
			 * TODO - Need to come up with a way to add updates to the DB schema 
			 * without effecting the users current db - some sort of migration is required.
			 * */
			if(!File.Exists(_foodDiaryDbConnection))
			{
				//Logger.Instance.Log ("Copying DB to personal folder");
				File.Copy (diaryDBPath, _foodDiaryDbConnection);
				//Logger.Instance.Log ("DB copied to personal folder");
			}
			if (!File.Exists (_foodBankDbConnection)) {
				File.Copy (shippedDBPath, _foodBankDbConnection);
			}
		}

		public static ProCalManager Instance
		{
			get
			{
				lock (_syncLock)
				{
					if (_instance == null) {
						_instance = new ProCalManager ();
					}
					return _instance;
				}

			}
		}

		public DateTime ResignTimeStamp{ get; set; }
		public DateTime CurrentSelectedDate{ get; set; }
		public User User{get;set;}
		public bool UpdateCalendar{ get; set; }
		public List<Food> AllFoods
		{
			get{ return _allFoods;}
		}
		public Dictionary<int, Food> AllFoodsDictionary
		{
			get{ return _allFoodsDictionary; }
		}

		public Dictionary<DateTime, List<FoodItem>> FoodItems
		{
			get{ return _foodItemsByDate; }
		}

		public Dictionary<DateTime, double> TotalProteinByDate
		{
			get{ return _totalSumProteinByDate; }
		}

		public Dictionary<DateTime, ProteinAward> ProteinAwards
		{
			get{ return _proteinAwards; }
		}

		public void Initialise()
		{
			if (_isInitialised) {
				return;
			}
			ProteinCalculator.CalculateDailyAwardRange (User.DailyProteinIntakeLimit);

			LoadFoodDatabase ();

			// sort by favourites then name
			AllFoods.Sort ();

			LoadFoodHistory ();

			_isInitialised = true;
		}

		public void AddFood(Food food)
		{
			using (SqliteConnection conn = new SqliteConnection("Data Source=" + _foodDiaryDbConnection))
			{
				using (SqliteCommand command = conn.CreateCommand ())
				{
					command.CommandText = "INSERT INTO Food(Name, ProteinGrams) VALUES (@foodname," + food.ProteinGrams + ");SELECT last_insert_rowid();";
					command.CommandType = CommandType.Text;
					command.Parameters.Add( new SqliteParameter( "@foodname", food.Name ) );
					conn.Open ();
					long id = (long) command.ExecuteScalar ();
					food.Id = (int)id;
					_allFoods.Add (food);
					_allFoodsDictionary.Add (food.Id, food);
				}
				conn.Close ();
			}
		}

		public void UpdateFood(Food food)
		{
			using (SqliteConnection conn = new SqliteConnection("Data Source=" + _foodDiaryDbConnection))
			{
				using (SqliteCommand command = conn.CreateCommand ())
				{
					command.CommandText = "UPDATE Food SET Name = '" + food.Name + "', ProteinGrams = " + food.ProteinGrams + " WHERE Id = " + food.Id;
					command.CommandType = CommandType.Text;
					conn.Open ();
					command.ExecuteNonQuery ();
				}
				conn.Close ();
			}

			if (FoodUpdated != null) {
				FoodUpdated (this, null);
			}
		}

		public void UpdateFavouriteCount(Food food)
		{
			food.FavouriteCount++;
			var path = food.IsCustom ? _foodDiaryDbConnection : _foodBankDbConnection;
			using (SqliteConnection conn = new SqliteConnection("Data Source=" + path))
			{
				using (SqliteCommand command = conn.CreateCommand ())
				{
					command.CommandText = "UPDATE Food SET FavouriteCount = " + food.FavouriteCount + " WHERE Id = " + food.Id;
					command.CommandType = CommandType.Text;
					conn.Open ();
					command.ExecuteNonQuery ();
				}
				conn.Close ();
			}
		}


		public void AddFoodItem(FoodItem item)
		{
			UpdateCalendar = true;
			using (SqliteConnection conn = new SqliteConnection("Data Source=" + _foodDiaryDbConnection))
			{
				using (SqliteCommand command = conn.CreateCommand ())
				{
					int isHistoric = item.IsHistoric ? 1 : 0;
					command.CommandText = "INSERT INTO FoodItems(FoodId, Servings, StartDate, IsHistoric) VALUES (" + item.Food.Id + "," + item.Servings.ToString(CultureInfo.InvariantCulture) + ",'" + item.StartDate.ToString(DATEFORMAT) + "'," + isHistoric + ");SELECT last_insert_rowid();";
					command.CommandType = CommandType.Text;
					conn.Open ();
					long id = (long) command.ExecuteScalar ();
					item.Id = id;
				}
				conn.Close ();
			}
			UpdateFoodDictionary (item, true);
		}

		private void LoadFoodDatabase()
		{
			/*
			 * SHIPPED FOOD DB - CANNOT BE EDITED
			 * */
			using(SqliteConnection conn = new SqliteConnection("Data Source=" + _foodBankDbConnection))
			{
				using(SqliteCommand command = conn.CreateCommand ())
				{
					command.CommandText = "SELECT Id, Name, ProteinGrams, FavouriteCount FROM Food";
					command.CommandType = CommandType.Text;
					conn.Open();
					using(SqliteDataReader dr = command.ExecuteReader ())
					{
						int idIdx = dr.GetOrdinal ("Id");
						int nameIdx = dr.GetOrdinal ("Name");
						int gramsIdx = dr.GetOrdinal ("ProteinGrams");
						int favCountIdx = dr.GetOrdinal ("FavouriteCount");
						while(dr.Read ())
						{
							Food food = new Food()
							{
								Id = dr.GetInt32(idIdx),
								Name = dr.GetString (nameIdx),
								ProteinGrams = dr.GetDouble(gramsIdx),
								FavouriteCount = dr.GetInt32(favCountIdx)
							};
							_allFoods.Add (food);
							_allFoodsDictionary.Add (food.Id, food);
						}

					}
				}
			}

			/*
			 * USERS FOOD DB - 
			 * */
			using(SqliteConnection conn = new SqliteConnection("Data Source=" + _foodDiaryDbConnection))
			{
				using(SqliteCommand command = conn.CreateCommand ())
				{
					command.CommandText = "SELECT Id, Name, ProteinGrams, FavouriteCount FROM Food";
					command.CommandType = CommandType.Text;
					conn.Open();
					using(SqliteDataReader dr = command.ExecuteReader ())
					{
						int idIdx = dr.GetOrdinal ("Id");
						int nameIdx = dr.GetOrdinal ("Name");
						int gramsIdx = dr.GetOrdinal ("ProteinGrams");
						int favCountIdx = dr.GetOrdinal ("FavouriteCount");
						while(dr.Read ())
						{

							Food food = new Food()
							{
								Id = dr.GetInt32(idIdx),
								Name = dr.GetString (nameIdx),
								ProteinGrams = dr.GetDouble(gramsIdx),
								IsCustom = true,
								FavouriteCount = dr.GetInt32(favCountIdx)
							};
							_allFoods.Add (food);
							_allFoodsDictionary.Add (food.Id, food);
						}

					}
				}
			}
		}

		private void LoadFoodHistory()
		{
			using(SqliteConnection conn = new SqliteConnection("Data Source=" + _foodDiaryDbConnection))
			{
				using(SqliteCommand command = conn.CreateCommand ())
				{
					command.CommandText = "SELECT Id, FoodId, Servings, StartDate, isHistoric FROM FoodItems";// WHERE Name LIKE '" + search +  "%'";
					command.CommandType = CommandType.Text;
					conn.Open();
					using(SqliteDataReader dr = command.ExecuteReader ())
					{
						int idIdx = dr.GetOrdinal ("Id");
						int foodIdx = dr.GetOrdinal ("FoodId");
						int servingsIdx = dr.GetOrdinal ("Servings");
						int startDateIdx = dr.GetOrdinal ("StartDate");
						int isHistIdx = dr.GetOrdinal ("isHistoric");
						while(dr.Read ())
						{

							string dateString = dr.GetString(startDateIdx);
							Console.WriteLine (dateString);
							var dateObj = ResolveDate (dateString);
							if (dateObj == null) {
								Console.WriteLine (dateString + " is broken");
								continue;
							}

							FoodItem foodItem = new FoodItem()
							{
								Id = dr.GetInt32(idIdx),
								Servings = dr.GetDouble(servingsIdx),
								StartDate = (DateTime)dateObj,
								IsHistoric = dr.GetBoolean(isHistIdx)
							};
							Food food;
							if (_allFoodsDictionary.TryGetValue (dr.GetInt32 (foodIdx), out food))
							{
								foodItem.Food = food;
							}
							UpdateFoodDictionary (foodItem, false);
						}
					}
				}
			}

			// loop through dictionary and update sum protein dict
			foreach (var pair in _foodItemsByDate) {
				double totalProtein = pair.Value.Sum (f => f.TotalProteinGrams);
				_totalSumProteinByDate.Add (pair.Key.Date, totalProtein);
				_proteinAwards.Add (pair.Key.Date, ProteinCalculator.CalculateAward (totalProtein));
			}
		}

		private DateTime? ResolveDate(string rawDate)
		{
			DateTime date;
			foreach (var format in _dateFormats) {
				if (DateTime.TryParseExact (rawDate, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out date)) {
					return date;
				}
			}
			return null;
		}

		public void DeleteFoodHistory(FoodItem item)
		{
			UpdateCalendar = true;
			using (SqliteConnection conn = new SqliteConnection("Data Source=" + _foodDiaryDbConnection))
			{
				using (SqliteCommand command = conn.CreateCommand ())
				{
					command.CommandText = "DELETE FROM FoodItems WHERE Id = " + item.Id;
					command.CommandType = CommandType.Text;
					conn.Open();
					command.ExecuteNonQuery ();
				}
				conn.Close();
			}
			List<FoodItem> foodItems;
			if (_foodItemsByDate.TryGetValue (item.StartDate.Date, out foodItems))
			{
				foodItems.Remove (item);
				double totalProtein;
				if(_totalSumProteinByDate.TryGetValue(item.StartDate.Date, out totalProtein))
				{
					totalProtein = foodItems.Sum (f => f.TotalProteinGrams);
					_totalSumProteinByDate [item.StartDate.Date] = totalProtein;
					ProteinAward award;
					if (_proteinAwards.TryGetValue (item.StartDate.Date, out award)) {
						_proteinAwards [item.StartDate.Date] = ProteinCalculator.CalculateAward (totalProtein);
					}
				}
			}

		}

		public void DeleteAllFoodHistory()
		{
			UpdateCalendar = true;
			using (SqliteConnection conn = new SqliteConnection("Data Source=" + _foodDiaryDbConnection))
			{
				using (SqliteCommand command = conn.CreateCommand ())
				{
					command.CommandText = "DELETE FROM FoodItems";
					command.CommandType = CommandType.Text;
					conn.Open();
					command.ExecuteNonQuery ();
				}
				conn.Close();
			}
			_foodItemsByDate.Clear ();
			_proteinAwards.Clear ();
			_totalSumProteinByDate.Clear ();
		}


		public void Save()
		{
			SaveUser (User);
		}

		public void SaveUser(User user)
		{
			if (user == null) {
				return;
			}
			User = user;
			try
			{
				using(Stream stream = File.Open(_sessionTokenPath, FileMode.Create))
				{
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.Serialize(stream, user);
				}
			}
			catch(Exception e)
			{
				//Logger.Instance.Log(e);
				Console.WriteLine(e.Message);
			}
		}

		public void DeleteUser()
		{
			if (File.Exists (_sessionTokenPath)) {
				File.Delete (_sessionTokenPath);
			}
			User = null;
		}

		public void Reset()
		{
			DeleteUser ();
			DeleteAllFoodHistory ();
		}

		private User GetUserFromToken()
		{
			User user = null;
			try
			{
				using(Stream stream = File.Open(_sessionTokenPath, FileMode.Open))
				{
					BinaryFormatter formatter = new BinaryFormatter();
					user = (User)formatter.Deserialize(stream);
				}
			}
			catch(Exception e)
			{
				//Logger.Instance.Log(e);
				Console.WriteLine(e.Message);
			}
			return user;
		}

		private void UpdateFoodDictionary(FoodItem foodItem, bool updateTotalProteinSum)
		{
			List<FoodItem> items;
			if (_foodItemsByDate.TryGetValue (foodItem.StartDate.Date, out items)) {
				items.Add (foodItem);
			}
			else{
				items = new List<FoodItem> ();
				items.Add (foodItem);
				_foodItemsByDate.Add (foodItem.StartDate.Date, items);
			}

			if (!updateTotalProteinSum) {
				return;
			}
			double total;
			double totalProtein = items.Sum (f => f.TotalProteinGrams);
			if (_totalSumProteinByDate.TryGetValue (foodItem.StartDate.Date, out total))
			{
				_totalSumProteinByDate [foodItem.StartDate.Date] = totalProtein;
			} 
			else{
				_totalSumProteinByDate.Add (foodItem.StartDate.Date, totalProtein);
			}
			ProteinAward tempAward;
			ProteinAward proteinAward = ProteinCalculator.CalculateAward (totalProtein);
			if (_proteinAwards.TryGetValue (foodItem.StartDate.Date, out tempAward)) {
				_proteinAwards [foodItem.StartDate.Date] = proteinAward;
			} else {
				_proteinAwards.Add (foodItem.StartDate.Date, proteinAward);
			}
		}
	}
}
