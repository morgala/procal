using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Back2Front.Animation;
using Back2Front.GUI;

namespace ProCal
{
	public enum SetupPhase
	{
		GenderView,
		WeightView,
		ActivityLevelView,
		FinalView
	}

	public partial class ProCalViewController : UIViewController
	{
		private User _user;
		private GenderView _genderView;
		private WeightView _weightView;
		private ActivityLevelView _activityLevelView;
		private DailyIntakeView _intakeView;
		private UISwipeGestureRecognizer _rightGest;

		private HorizontalBounceAnimation _genderViewAnim;
		private HorizontalBounceAnimation _weightViewAnim;
		private HorizontalBounceAnimation _levelViewAnim;
		private HorizontalBounceAnimation _intakeViewAnim;

		private float _originalGenderViewCenterX;
		private RectangleF _orgGenderViewFrame;

		private float _originalWeightViewCenterX;
		private RectangleF _orgWeightViewFrame;
		private float _orgActivityViewCenterX;
		private RectangleF _orgActivityViewFrame;
		private float _orgIntakeViewCenterX;
		private RectangleF _orgIntakeViewFrame;

		private bool _isWeightAnimRunning;
		private bool _isGenderAnimRunning;
		private bool _isLevelAnimRunning;
		private bool _isIntakeAnimRunning;

		public ProCalViewController (IntPtr handle) : base (handle)
		{
		}

		public SetupPhase SetupPhase{ get; private set; }


		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		#region View lifecycle
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

//			if(UIScreen.MainScreen.Bounds.Height == 568)
//			{
//				backgroundImageView.Image = UIImage ("bg");
//			}

			SetupPhase = SetupPhase.GenderView;
			ProCalManager.Instance.User = User.CreateUser ();
			_user = ProCalManager.Instance.User;
			backButton.Hidden = true;
			backButton.TouchUpInside += BackButtonTouched;
			skipButton.TouchUpInside += OnSetupTouch;

			/* GENDER VIEW */
			_genderView = GenderView.GetView ();
			backgroundImageView.AddSubview (_genderView);
			_genderView.Frame = new RectangleF((UIScreen.MainScreen.Bounds.Width - _genderView.Frame.Width)/2 , (UIScreen.MainScreen.Bounds.Height - _genderView.Frame.Height)/2, _genderView.Frame.Width, _genderView.Frame.Height);
			_genderView.GenderSelected += OnGenderSelected;
			_originalGenderViewCenterX = _genderView.Center.X;
			_orgGenderViewFrame = _genderView.Frame;


			/* WEIGHT VIEW */
			_weightView = WeightView.GetView ();
			backgroundImageView.AddSubview (_weightView);
			_weightView.Frame = new RectangleF((UIScreen.MainScreen.Bounds.Width - _weightView.Frame.Width)/2 , (UIScreen.MainScreen.Bounds.Height - _weightView.Frame.Height)/2, _weightView.Frame.Width, _weightView.Frame.Height);
			_weightView.Hidden = true;
			_weightView.WeightSelected += OnWeightSelected;
			_originalWeightViewCenterX = _weightView.Center.X;
			_orgWeightViewFrame = _weightView.Frame;

			/* ACTIVITY LEVEL */
			_activityLevelView = ActivityLevelView.GetView ();
			backgroundImageView.AddSubview (_activityLevelView);
			_activityLevelView.Frame = new RectangleF((UIScreen.MainScreen.Bounds.Width - _activityLevelView.Frame.Width)/2 , (UIScreen.MainScreen.Bounds.Height - _activityLevelView.Frame.Height)/2, _activityLevelView.Frame.Width, _activityLevelView.Frame.Height);
			_activityLevelView.Hidden = true;
			_activityLevelView.ActivityLevelSelected += OnLevelSelected;
			_orgActivityViewCenterX = _activityLevelView.Center.X;
			_orgActivityViewFrame = _activityLevelView.Frame;

			/* INTAKE VIEW */
			_intakeView = DailyIntakeView.GetView ();
			backgroundImageView.AddSubview (_intakeView);
			_intakeView.Frame = new RectangleF((UIScreen.MainScreen.Bounds.Width - _intakeView.Frame.Width)/2 , (UIScreen.MainScreen.Bounds.Height - _intakeView.Frame.Height)/2, _intakeView.Frame.Width, _intakeView.Frame.Height);
			_intakeView.Hidden = true;
			_intakeView.Close += OnIntakeClose;
			_orgIntakeViewCenterX = _intakeView.Center.X;
			_orgIntakeViewFrame = _intakeView.Frame;

			_rightGest = new UISwipeGestureRecognizer (() => {
				switch(SetupPhase)
				{
				case SetupPhase.WeightView:
					if(_isWeightAnimRunning)
					{
						return;
					}
					SetupPhase = SetupPhase.GenderView;
					AnimateWeightView (UISwipeGestureRecognizerDirection.Right, true);
					AnimateGenderView(UISwipeGestureRecognizerDirection.Right);
					break;
				case SetupPhase.ActivityLevelView:
					if(_isLevelAnimRunning)
					{
						return;
					}
					SetupPhase = SetupPhase.WeightView;
					AnimateLevelView(UISwipeGestureRecognizerDirection.Right, true);
					AnimateWeightView(UISwipeGestureRecognizerDirection.Left, true);
					break;
				case SetupPhase.FinalView:
					if(_isIntakeAnimRunning)
					{
						return;
					}
					SetupPhase = SetupPhase.ActivityLevelView;
					AnimateDailyIntakeView(UISwipeGestureRecognizerDirection.Right);
					AnimateLevelView(UISwipeGestureRecognizerDirection.Left, true);
					break;

				}
			});
			_rightGest.Direction = UISwipeGestureRecognizerDirection.Right;
			backgroundImageView.AddGestureRecognizer (_rightGest);
		}

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			View.EndEditing(true);
		}

		#endregion

		private void OnGenderSelected (object sender, EventArgs e)
		{
			if (_isGenderAnimRunning) {
				return;
			}
			backgroundImageView.Image = _user.Sex == Sex.Male ? UIImage.FromFile ("bg.png") : UIImage.FromFile ("bg-female.png");
			SetupPhase = SetupPhase.WeightView;
			AnimateGenderView (UISwipeGestureRecognizerDirection.Left);
			AnimateWeightView (UISwipeGestureRecognizerDirection.Right);
		}

		private void OnWeightSelected (object sender, EventArgs e)
		{
			if (_isWeightAnimRunning) {
				return;
			}
			SetupPhase = SetupPhase.ActivityLevelView;
			AnimateWeightView (UISwipeGestureRecognizerDirection.Left);
			AnimateLevelView (UISwipeGestureRecognizerDirection.Right);
		}

		private void OnLevelSelected (object sender, EventArgs e)
		{
			if (_isLevelAnimRunning) {
				return;
			}
			SetupPhase = SetupPhase.FinalView;
			AnimateLevelView (UISwipeGestureRecognizerDirection.Left);
			AnimateDailyIntakeView (UISwipeGestureRecognizerDirection.Left);
			_intakeView.Update ();
		}

		private void OnIntakeClose (object sender, EventArgs e)
		{
			ProCalManager.Instance.SaveUser (_user);
			ProCalManager.Instance.Initialise ();
			PerformSegue("TabBarSegue", this);
		}

		private void AnimateGenderView(UISwipeGestureRecognizerDirection direction)
		{
			float fromValue;
			float toValue;
			if (direction == UISwipeGestureRecognizerDirection.Right) {
				// going back so need to hide view.
				fromValue = 0;
				toValue = _originalGenderViewCenterX;
				_genderView.Frame = _orgGenderViewFrame;
			}
			else
			{
				fromValue = _genderView.Center.X;
				toValue = -_genderView.Frame.Width;

				var frame = _genderView.Frame;
				frame.X = toValue;
				_genderView.Frame = frame;
			}
			_isGenderAnimRunning = true;
			_genderViewAnim = new HorizontalBounceAnimation ("animHoriz", _genderView)
			{
				FromValue = new NSNumber(fromValue),
				ToValue = new NSNumber(toValue),
				NumberOfBounces = 4,
				Duration = 1f,
				ShouldOvershoot = true
			};
			_genderView.Layer.RemoveAnimation (_genderViewAnim.Key);
			_genderViewAnim.Initialise ();
			_genderViewAnim.AnimationCompleted += (sender, e) => _isGenderAnimRunning = false;
			_genderView.Layer.AddAnimation (_genderViewAnim.Animation, _genderViewAnim.Key);
		}

		private void AnimateWeightView(UISwipeGestureRecognizerDirection direction, bool goBack=false)
		{
			float fromValue;
			float toValue;

			if (direction == UISwipeGestureRecognizerDirection.Right)
			{
				if (goBack) {
					fromValue = _weightView.Center.X;
					toValue = UIScreen.MainScreen.Bounds.Width + _weightView.Frame.Width;

					var frame = _weightView.Frame;
					frame.X = toValue;
					_weightView.Frame = frame;

				}
				else
				{
					fromValue = UIScreen.MainScreen.Bounds.Width;
					toValue = _originalWeightViewCenterX;
					_weightView.Frame = _orgWeightViewFrame;
				}
			} 
			else
			{
				if (goBack) {
					fromValue = 0;
					toValue = _originalWeightViewCenterX;
					_weightView.Frame = _orgWeightViewFrame;
				} else {
					fromValue = _weightView.Center.X;
					toValue = -_weightView.Frame.Width;

					var frame = _weightView.Frame;
					frame.X = toValue;
					_weightView.Frame = frame;
				}
			}
			_weightView.Hidden = false;
			_isWeightAnimRunning = true;
			_weightViewAnim = new HorizontalBounceAnimation ("animHoriz", _weightView)
			{
				FromValue = new NSNumber(fromValue),
				ToValue = new NSNumber(toValue),
				NumberOfBounces = 4,
				Duration = 1f,
				ShouldOvershoot = true
			};
			_weightView.Layer.RemoveAnimation (_weightViewAnim.Key);
			_weightViewAnim.AnimationCompleted += (sender, e) => _isWeightAnimRunning = false;
			_weightViewAnim.Initialise ();
			_weightView.Layer.AddAnimation (_weightViewAnim.Animation, _weightViewAnim.Key);
		}

		private void AnimateLevelView(UISwipeGestureRecognizerDirection direction, bool goBack=false)
		{
			float fromValue;
			float toValue;
			if (direction == UISwipeGestureRecognizerDirection.Right)
			{
				if (goBack) {
					fromValue = _activityLevelView.Center.X;
					toValue = UIScreen.MainScreen.Bounds.Width + _activityLevelView.Frame.Width;

					var frame = _activityLevelView.Frame;
					frame.X = toValue;
					_activityLevelView.Frame = frame;

				}
				else
				{
					fromValue = UIScreen.MainScreen.Bounds.Width;
					toValue = _orgActivityViewCenterX;
					_activityLevelView.Frame = _orgActivityViewFrame;
				}
			} 
			else
			{
				if (goBack) {
					fromValue = 0;
					toValue = _orgActivityViewCenterX;
					_activityLevelView.Frame = _orgActivityViewFrame;
				} else {
					fromValue = _activityLevelView.Center.X;
					toValue = -_activityLevelView.Frame.Width;

					var frame = _activityLevelView.Frame;
					frame.X = toValue;
					_activityLevelView.Frame = frame;
				}
			}

			_isLevelAnimRunning = true;
			_activityLevelView.Hidden = false;
			_levelViewAnim = new HorizontalBounceAnimation ("animHoriz", _activityLevelView)
			{
				FromValue = new NSNumber(fromValue),
				ToValue = new NSNumber(toValue),
				NumberOfBounces = 4,
				Duration = 1f,
				ShouldOvershoot = true
			};
			_activityLevelView.Layer.RemoveAnimation (_levelViewAnim.Key);
			_levelViewAnim.AnimationCompleted += (sender, e) => _isLevelAnimRunning = false;
			_levelViewAnim.Initialise ();
			_activityLevelView.Layer.AddAnimation (_levelViewAnim.Animation, _levelViewAnim.Key);
		}

		private void AnimateDailyIntakeView(UISwipeGestureRecognizerDirection direction)
		{
			float fromValue;
			float toValue;
			if (direction == UISwipeGestureRecognizerDirection.Right)
			{
				fromValue = _intakeView.Center.X;
				toValue = UIScreen.MainScreen.Bounds.Width + _intakeView.Frame.Width;

				var frame = _intakeView.Frame;
				frame.X = toValue;
				_intakeView.Frame = frame;
			}
			else
			{
				fromValue = UIScreen.MainScreen.Bounds.Width;
				toValue = _orgIntakeViewCenterX;
				_intakeView.Frame = _orgIntakeViewFrame;
			}

			_isIntakeAnimRunning = true;
			_intakeView.Hidden = false;
			_intakeViewAnim = new HorizontalBounceAnimation ("animHoriz", _intakeView)
			{
				FromValue = new NSNumber(UIScreen.MainScreen.Bounds.Width),
				ToValue = new NSNumber(_intakeView.Center.X),
				NumberOfBounces = 4,
				Duration = 1f,
				ShouldOvershoot = true
			};
			_intakeViewAnim.Initialise ();
			_intakeViewAnim.AnimationCompleted += (sender, e) => _isIntakeAnimRunning = false;
			_intakeView.Layer.AddAnimation (_intakeViewAnim.Animation, _intakeViewAnim.Key);
		}

		private void BackButtonTouched (object sender, EventArgs e)
		{

		}

		private void OnSetupTouch (object sender, EventArgs e)
		{
			try{

				ProteinCalculator.CalculateDailyIntake(_user);
				ProCalManager.Instance.SaveUser (_user);
				ProCalManager.Instance.Initialise ();
				PerformSegue("TabBarSegue", this);
			}
			catch(Exception ex) {
				Console.WriteLine (ex.Message);
			}
		}
	}
}

