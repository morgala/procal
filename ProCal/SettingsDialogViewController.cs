using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;

namespace ProCal
{
	public partial class SettingsDialogViewController : DialogViewController
	{
		private UIView _dismiss;

		public SettingsDialogViewController (RootElement root, bool pushing) : base(root, pushing)
		{
		}

		public override void LoadView ()
		{
			base.LoadView ();

		}

		public override UIView InputAccessoryView 
		{
			get 
			{
				if (_dismiss == null)
				{
					_dismiss = new UIView(new RectangleF(0,0,320,44));
					_dismiss.BackgroundColor = UIColor.White.ColorWithAlpha (0.95f);
					UIButton dismissBtn = UIButton.FromType (UIButtonType.System);
					dismissBtn.SetTitleColor (UIColor.FromRGB (80, 169, 237), UIControlState.Normal);
					dismissBtn.SetTitleColor (UIColor.White, UIControlState.Highlighted);
					dismissBtn.TintColor = UIColor.White;
					dismissBtn.Frame = new RectangleF (240, 0, 58, 44);
					dismissBtn.SetTitle ("Done", UIControlState.Normal);
					dismissBtn.TouchUpInside  += delegate {
						View.EndEditing(true);
					};
					_dismiss.AddSubview(dismissBtn);
				}
				return _dismiss;
			}
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			TableView.BackgroundView = null;
			UIImage backImage = ProCalManager.Instance.User.Sex == Sex.Male ? UIImage.FromFile ("bg.png") : UIImage.FromFile ("bg-female.png");
			var background = backImage;
			TableView.BackgroundView = new UIImageView(background);
		}
	}
}
