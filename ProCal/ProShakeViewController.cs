using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.AudioToolbox;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;

using Back2Front.Animation;
using Back2Front.Utilities;

namespace ProCal
{
	public partial class ProShakeViewController : UIViewController
	{
		private CalendarWeekView _weekView;
		private RectangleF _shakeViewOrgFrame;
		private RectangleF _shakeLabelOrgFrame;
		private RectangleF _shakeViewCurrentFrame;
		private RectangleF _shakeLabelCurrentFrame;
		private int proteinLevel = 0;
		private UIPanGestureRecognizer _panGest;
		private NSTimer _startTimer;
		private NSTimer _stopTimer;
		private float MAX_LEVEL_Y;
		private float MIN_LEVEL_Y;
		private List<Interval> _intervals;
		private float _orginalFontSize;
		private bool _hasUserDragged;
		private DragLevelView _dragPromptView;
		private float _proteinSegmentPixelHeight;
		private User _user;


		public ProShakeViewController (IntPtr handle) : base (handle)
		{
		}

		public override bool CanBecomeFirstResponder {
			get {
				return true;
			}
		}

		public bool IsIPhone5
		{
			get{
				return UIScreen.MainScreen.Bounds.Height == 568;
			}

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			_weekView = new CalendarWeekView(new RectangleF(0, 20, 320, 100), true, false);
			View.AddSubview (_weekView);
			shakeLevelView.ClipsToBounds = true;
			_orginalFontSize = shakeLevelLabel.Font.PointSize;
			resetButton.TouchUpInside += (sender, e) => {
				if (proteinLevel == 0) {
					return;
				}
				ResetLevel ();
			};
			addButton.TouchUpInside += AddFoodItem;

			NSNotificationCenter.DefaultCenter.AddObserver (UIApplication.WillEnterForegroundNotification, (n) => 
			{
				try
				{
					if(DateTime.Today > ProCalManager.Instance.ResignTimeStamp.Date)
					{
						UpdateCalendar();
						_weekView.UpdateDateLabel(DateTime.Now);
					}
				}
				catch(Exception e)
				{
					Console.WriteLine(e.Message);
				}
			});

			/* 
			 * TAP GEST TO DISMISS SHAKE PROMPT
			 * */
			var tap = new UITapGestureRecognizer ();
			tap.CancelsTouchesInView = false;
			tap.AddTarget (() =>{
				StopShakeTimers();
			});
			promptImageView.AddGestureRecognizer (tap);
			promptImageView.Hidden = true;

			/*
			 * IPHONE 5
			 * */
			if (IsIPhone5)
			{
				_proteinSegmentPixelHeight = 32;
				scaleImageView.Frame = new RectangleF (265, 185, 55, 333);
				scaleImageView.Image = UIImage.FromFile ("shake-scale.png");
				var addFrame = addButton.Frame;
				addFrame.Y += 7;
				addButton.Frame = addFrame;
				var resetFrame = resetButton.Frame;
				resetFrame.Y += 7;
				resetButton.Frame = resetFrame;

				MIN_LEVEL_Y = 190f;
				MAX_LEVEL_Y = 478f; //478f
				_intervals = new List<Interval> (10);
				_intervals.Add (new Interval (){ Low = 447, High = MAX_LEVEL_Y, ProteinLevel = 5 });
				_intervals.Add (new Interval (){ Low = 415f, High = 446f, ProteinLevel = 10 });
				_intervals.Add (new Interval (){ Low = 383f, High = 414f, ProteinLevel = 15 });
				_intervals.Add (new Interval (){ Low = 351f, High = 382f, ProteinLevel = 20 });
				_intervals.Add (new Interval (){ Low = 319f, High = 350f, ProteinLevel = 25 });
				_intervals.Add (new Interval (){ Low = 287f, High = 318f, ProteinLevel = 30 });
				_intervals.Add (new Interval (){ Low = 255f, High = 286f, ProteinLevel = 35 });
				_intervals.Add (new Interval (){ Low = 223f, High = 254f, ProteinLevel = 40 });
				_intervals.Add (new Interval (){ Low = 191f, High = 222f, ProteinLevel = 45 });
				_intervals.Add (new Interval (){ Low = 0.0f, High = MIN_LEVEL_Y, ProteinLevel = 50 });
			}
			else
			{
				/* IPHONE 4 AND BEFORE ...*/
				_proteinSegmentPixelHeight = 25;
				scaleImageView.Frame = new RectangleF (265, 177, 55, 255);
				scaleImageView.Image = UIImage.FromFile ("shake-scale-small.png");

				MIN_LEVEL_Y = 181f;
				MAX_LEVEL_Y = 406f;
				_intervals = new List<Interval> (10);
				_intervals.Add (new Interval (){ Low = 382f, High = MAX_LEVEL_Y, ProteinLevel = 5 });
				_intervals.Add (new Interval (){ Low = 357f, High = 381f, ProteinLevel = 10 });
				_intervals.Add (new Interval (){ Low = 332f, High = 356f, ProteinLevel = 15 });
				_intervals.Add (new Interval (){ Low = 307f, High = 331f, ProteinLevel = 20 });
				_intervals.Add (new Interval (){ Low = 282f, High = 306f, ProteinLevel = 25 });
				_intervals.Add (new Interval (){ Low = 257f, High = 281f, ProteinLevel = 30 });
				_intervals.Add (new Interval (){ Low = 232f, High = 256f, ProteinLevel = 35 });
				_intervals.Add (new Interval (){ Low = 207f, High = 231f, ProteinLevel = 40 });
				_intervals.Add (new Interval (){ Low = 182f, High = 206f, ProteinLevel = 45 });
				_intervals.Add (new Interval (){ Low = 0.0f, High = MIN_LEVEL_Y, ProteinLevel = 50 });

			}
			_shakeViewOrgFrame = shakeLevelView.Frame;
			_shakeLabelOrgFrame = shakeLevelLabel.Frame;

			/*
			 * DRAG PROMPT VIEW
			 * */
			_dragPromptView = DragLevelView.GetView ();
			_dragPromptView.Frame = new RectangleF((UIScreen.MainScreen.Bounds.Width - scaleImageView.Frame.Width - _dragPromptView.Frame.Width)/2 , 
				MAX_LEVEL_Y - 150, 
				_dragPromptView.Frame.Width, 
				_dragPromptView.Frame.Height);
			View.AddSubview (_dragPromptView);
			_dragPromptView.Alpha = 0;
			var tap2 = new UITapGestureRecognizer ();
			tap2.CancelsTouchesInView = false;
			tap2.AddTarget(()=> _dragPromptView.Alpha = 0);
			_dragPromptView.AddGestureRecognizer (tap2);

			/*
			 * DRAG PAN GESTURE
			 * */
			_panGest = new UIPanGestureRecognizer (() => {

				_dragPromptView.Alpha = 0;
				PointF movement = _panGest.TranslationInView(shakeLevelView);
				RectangleF newViewFrame = new RectangleF(shakeLevelView.Frame.Location.X, 
					shakeLevelView.Frame.Location.Y + movement.Y, 
					shakeLevelView.Frame.Size.Width, 
					shakeLevelView.Frame.Size.Height - movement.Y);

				RectangleF newLabelFrame = new RectangleF(shakeLevelLabel.Frame.Location.X, 
					shakeLevelLabel.Frame.Location.Y + movement.Y, 
					shakeLevelLabel.Frame.Size.Width, 
					shakeLevelLabel.Frame.Size.Height - movement.Y);

				if((newViewFrame.Location.Y >= MIN_LEVEL_Y && newViewFrame.Y <= MAX_LEVEL_Y) || (newViewFrame.Y < MIN_LEVEL_Y))
				{

					if((newViewFrame.Y < MIN_LEVEL_Y))
					{
						newViewFrame.Y = MIN_LEVEL_Y;
						newLabelFrame = shakeLevelLabel.Frame;
					}

					_hasUserDragged = true;
					shakeLevelView.Frame = newViewFrame;
					shakeLevelLabel.Frame = newLabelFrame;
					_shakeViewCurrentFrame = shakeLevelView.Frame;
					_shakeLabelCurrentFrame = shakeLevelLabel.Frame;
					_panGest.SetTranslation(Point.Empty, View);

					// increment/decrement protein level
					var interval = _intervals.Find(i => i.Low <= newViewFrame.Location.Y && i.High >= newViewFrame.Location.Y);
					if(interval != null)
					{
						if(proteinLevel != interval.ProteinLevel)
						{
							proteinLevel = interval.ProteinLevel;
							float fontSize = proteinLevel / 5;
							shakeLevelLabel.Alpha = 0;
							UIView.Animate (0.3, 0.0, UIViewAnimationOptions.CurveLinear, () => {
								shakeLevelLabel.Alpha = 1;
								shakeLevelLabel.Font = UIFont.SystemFontOfSize(_orginalFontSize + fontSize*3);
								shakeLevelLabel.Text = string.Format("{0}g", proteinLevel);
							}, null);
						}
					}
				}
			});

			_panGest.MaximumNumberOfTouches = 1;
			shakeLevelView.AddGestureRecognizer (_panGest);

		}



		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			this.BecomeFirstResponder ();
			_startTimer = NSTimer.CreateRepeatingScheduledTimer (1, () => {
				StartShakePrompt();
			});
			_stopTimer = NSTimer.CreateRepeatingScheduledTimer (3, () => {
				StopShakePrompt();
			});
			_hasUserDragged = false;
		}

		public override void ViewWillDisappear (bool animated)
		{
			this.ResignFirstResponder ();
			base.ViewWillDisappear (animated);
			ResetLevel ();
			StopShakeTimers();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			_user = ProCalManager.Instance.User;
			backgroundImageView.Image = _user.Sex == Sex.Male ? UIImage.FromFile ("bg.png") : UIImage.FromFile ("bg-female.png");
			UpdateCalendar ();
			_weekView.UpdateDateLabel(DateTime.Now);

		}

		public override void MotionEnded (UIEventSubtype motion, UIEvent evt)
		{
			base.MotionEnded (motion, evt);
			IncreaseLevel ();
			StopShakeTimers ();
		}

		private void IncreaseLevel()
		{
			_dragPromptView.Alpha = 0;
			_shakeViewCurrentFrame = shakeLevelView.Frame;
			_shakeLabelCurrentFrame = shakeLevelLabel.Frame;
			if (_hasUserDragged)
			{
				_hasUserDragged = false;
				int tempLevel = proteinLevel + 5;
				if (tempLevel == 55) {
					ResetLevel ();
					return;
				}
				// here we deduce where they are in the scale get current level and take Low
				var interval =_intervals.Find(i => i.ProteinLevel == proteinLevel);
				_shakeViewCurrentFrame = new RectangleF(
					_shakeViewCurrentFrame.Location.X, 
					interval.High, 
					_shakeViewCurrentFrame.Size.Width, 
					_shakeViewCurrentFrame.Size.Height - ( interval.High - _shakeViewCurrentFrame.Location.Y));

				_shakeLabelCurrentFrame = new RectangleF(_shakeLabelCurrentFrame.Location.X, interval.High, _shakeLabelCurrentFrame.Size.Width,
					_shakeLabelCurrentFrame.Size.Height - (interval.High - _shakeLabelCurrentFrame.Location.Y));
			}

			proteinLevel += 5;
			if (proteinLevel == 55) {
				ResetLevel ();
				return;
			}

			float fontSize = proteinLevel / 5;
			shakeLevelLabel.Alpha = 0;
			UIView.Animate (0.5, 0.2, UIViewAnimationOptions.CurveLinear, () => {
				var segmentIncrement = _proteinSegmentPixelHeight;
				if(IsIPhone5 && proteinLevel == 5)
				{
					segmentIncrement+=9;
				}

				shakeLevelView.Frame = new RectangleF(_shakeViewCurrentFrame.Location.X, _shakeViewCurrentFrame.Location.Y - segmentIncrement, _shakeViewCurrentFrame.Size.Width, _shakeViewCurrentFrame.Size.Height + segmentIncrement);
				shakeLevelLabel.Frame = new RectangleF(_shakeLabelCurrentFrame.Location.X, _shakeLabelCurrentFrame.Location.Y - segmentIncrement, _shakeLabelCurrentFrame.Size.Width, _shakeLabelCurrentFrame.Size.Height + segmentIncrement);
				shakeLevelLabel.Alpha = 1;
				shakeLevelLabel.Font = UIFont.SystemFontOfSize(_orginalFontSize + fontSize*3);
				shakeLevelLabel.Text = string.Format("{0}g", proteinLevel);
				if (proteinLevel == 5) {
					_dragPromptView.Alpha = 1;
					_dragPromptView.Animate();
				}
			}, null);
			SystemSound.Vibrate.PlaySystemSound ();
		}

		private void UpdateCalendar()
		{
			_weekView.MoveToToday();
			_weekView.RefreshWeek();
		}

		private void ResetLevel()
		{
			_hasUserDragged = false;
			_dragPromptView.Alpha = 0;
			if (proteinLevel == 0) {
				return;
			}
			shakeLevelLabel.Alpha = 0;
			shakeLevelLabel.Font = UIFont.SystemFontOfSize(17);
			UIView.Animate (1.0, 0.0, UIViewAnimationOptions.CurveLinear, () => {
				shakeLevelView.Frame = new RectangleF(_shakeViewCurrentFrame.Location.X, _shakeViewOrgFrame.Location.Y, _shakeViewCurrentFrame.Size.Width, _shakeViewOrgFrame.Size.Height);
				shakeLevelLabel.Frame = new RectangleF(_shakeLabelCurrentFrame.Location.X, _shakeLabelOrgFrame.Location.Y, _shakeLabelCurrentFrame.Size.Width, _shakeLabelOrgFrame.Size.Height);
			}, null);
			proteinLevel = 0;
		}

		private void AddFoodItem (object sender, EventArgs e)
		{
			try
			{
				if(proteinLevel == 0)
				{
					return;
				}
				Food food;
				if(ProCalManager.Instance.AllFoodsDictionary.TryGetValue(8, out food))
				{
					FoodItem item = new FoodItem();
					item.Food = food;
					item.Servings = (double)proteinLevel/20;
					item.StartDate = DateTime.Now;
					ProCalManager.Instance.AddFoodItem(item);
					var message = string.Format("{0}g of protein shake has been added to your log!", proteinLevel);
					var alert = new UIAlertView("Food Logged", message, null, "OK",null);
					alert.Show();
					UpdateCalendar();
				}
				ResetLevel ();

			}
			catch(Exception ex)
			{
				Console.WriteLine (ex.Message);
				var errorView = new UIAlertView ("Error", "Oh oh there was an error!", null, "OK", null);
				errorView.Show ();
			}
		}

		private void StartShakePrompt()
		{
			promptImageView.Hidden = false;
			promptImageView.Transform = CGAffineTransform.MakeRotation ((float)(-5 * Math.PI) / 180f);

			UIView.Animate (0.1f, 0, 
				UIViewAnimationOptions.AllowUserInteraction | 
				UIViewAnimationOptions.Repeat | 
				UIViewAnimationOptions.Autoreverse,
				() => {
					promptImageView.Transform = CGAffineTransform.MakeRotation ((float)(5f * Math.PI) / 180f);
				}, null);
		}

		private void StopShakePrompt()
		{
			UIView.Animate (0.1f, 0, 
				UIViewAnimationOptions.AllowUserInteraction | 
				UIViewAnimationOptions.BeginFromCurrentState | 
				UIViewAnimationOptions.CurveLinear,
				() => {
					promptImageView.Transform = CGAffineTransform.MakeIdentity();
				}, null);
		}

		private void StopShakeTimers()
		{
			promptImageView.Hidden = true;
			if (_startTimer != null) {
				_startTimer.Dispose ();
				_startTimer = null;
			}
			if (_stopTimer != null) {
				_stopTimer.Dispose ();
				_stopTimer = null;
			}
		}

		public class Interval
		{
			public float Low{get;set;}
			public float High{ get; set; }
			public int ProteinLevel{get;set;}
		}

	}
}
