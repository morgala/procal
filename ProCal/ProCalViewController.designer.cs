// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("ProCalViewController")]
	partial class ProCalViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton backButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView backgroundImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton changeWeightButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton goButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel proteinGramsPerLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISlider proteinGramsSlider { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel proteinLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton skipButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIPickerView weightPickerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel welcomeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backButton != null) {
				backButton.Dispose ();
				backButton = null;
			}

			if (backgroundImageView != null) {
				backgroundImageView.Dispose ();
				backgroundImageView = null;
			}

			if (changeWeightButton != null) {
				changeWeightButton.Dispose ();
				changeWeightButton = null;
			}

			if (goButton != null) {
				goButton.Dispose ();
				goButton = null;
			}

			if (proteinGramsPerLabel != null) {
				proteinGramsPerLabel.Dispose ();
				proteinGramsPerLabel = null;
			}

			if (proteinGramsSlider != null) {
				proteinGramsSlider.Dispose ();
				proteinGramsSlider = null;
			}

			if (proteinLabel != null) {
				proteinLabel.Dispose ();
				proteinLabel = null;
			}

			if (skipButton != null) {
				skipButton.Dispose ();
				skipButton = null;
			}

			if (weightPickerView != null) {
				weightPickerView.Dispose ();
				weightPickerView = null;
			}

			if (welcomeLabel != null) {
				welcomeLabel.Dispose ();
				welcomeLabel = null;
			}
		}
	}
}
