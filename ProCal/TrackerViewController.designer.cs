// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("TrackerViewController")]
	partial class TrackerViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton addFoodButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView backgroundImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIProgressView bronzeProgressBar { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView containerView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel dailyTotalLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton expandViewButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView foodTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIProgressView goldProgressBar { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIProgressView silverProgressBar { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton todayButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backgroundImageView != null) {
				backgroundImageView.Dispose ();
				backgroundImageView = null;
			}

			if (addFoodButton != null) {
				addFoodButton.Dispose ();
				addFoodButton = null;
			}

			if (bronzeProgressBar != null) {
				bronzeProgressBar.Dispose ();
				bronzeProgressBar = null;
			}

			if (containerView != null) {
				containerView.Dispose ();
				containerView = null;
			}

			if (dailyTotalLabel != null) {
				dailyTotalLabel.Dispose ();
				dailyTotalLabel = null;
			}

			if (expandViewButton != null) {
				expandViewButton.Dispose ();
				expandViewButton = null;
			}

			if (foodTable != null) {
				foodTable.Dispose ();
				foodTable = null;
			}

			if (goldProgressBar != null) {
				goldProgressBar.Dispose ();
				goldProgressBar = null;
			}

			if (silverProgressBar != null) {
				silverProgressBar.Dispose ();
				silverProgressBar = null;
			}

			if (todayButton != null) {
				todayButton.Dispose ();
				todayButton = null;
			}
		}
	}
}
