using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ProCal
{
	public partial class SetUpSecondStageViewController : UIViewController
	{
		private User _user;

		public SetUpSecondStageViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			_user = ProCalManager.Instance.User;

			proteinGramsSlider.ValueChanged += (sender, e) => 
			{
				CalculateDailyIntake();
			};

			goButton.TouchUpInside += OnSetupTouch;
			skipButton.TouchUpInside += OnSetupTouch;
		}

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			CalculateDailyIntake ();
		}

		private void CalculateDailyIntake()
		{
			proteinLabel.Text = string.Format ("{0}g", _user.DailyProteinIntakeLimit);
		}

		private void OnSetupTouch (object sender, EventArgs e)
		{
			try{
				// TODO: possibly showing loading view????
				// at this point we're only loading the static food DB.
				ProCalManager.Instance.Initialise ();
				ProCalManager.Instance.SaveUser (_user);

				PerformSegue("TabBarSegue2", this);
			}
			catch(Exception ex) {
				Console.WriteLine (ex.Message);
			}
		}
	}
}
