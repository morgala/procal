using System;
using System.Drawing;
using MonoTouch.Dialog;
using MonoTouch.Foundation;
using MonoTouch.MessageUI;
using MonoTouch.UIKit;
using Back2Front.GUI;

namespace ProCal
{
	public partial class SettingsNavController : UINavigationController
	{
		private User _user;
		private RootElement _root;
		private SettingsDialogViewController _dv;
		private EntryElementAdv _weightElement;
		private BooleanElementAdv _notificationElement;
		private ResetAlertViewDelegate _alertDelegate;
		private EntryElementAdv _dailyIntakeElement;
		private ClickableRadioElement _maleRadioElement;
		private ClickableRadioElement _femaleRadioElement;
		private ClickableRadioElement _metricRadioElement;
		private ClickableRadioElement _imperialRadioElement;

		private ClickableRadioElement _sedRadElement;
		private ClickableRadioElement _lightRadElement;
		private ClickableRadioElement _modRadElement;
		private ClickableRadioElement _veryActiveRadElement;
		private ClickableRadioElement _heavyRadElement;
		private UIImage _backImage;

		private BackgoundImageRootElement _sexImage;
		private BackgoundImageRootElement _weightImage;
		private BackgoundImageRootElement _levelImage;
		private BackgoundImageRootElement _notificationImage;
		private BackgoundImageRootElement _resetImage;

		private UIButton _emailButton;
		private UIButton _fbButton;

		public event EventHandler<SettingsCloseArgs> Close;

		public SettingsNavController (IntPtr handle) : base (handle)
		{
			_user = ProCalManager.Instance.User;
		}

		public SettingsNavController () : base ("FoodPickerController", null)
		{
			_user = ProCalManager.Instance.User;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_alertDelegate = new ResetAlertViewDelegate(this);

			/*******  SEX ********/
			_maleRadioElement = new ClickableRadioElement (Sex.Male.ToString ());
			_maleRadioElement.OnSelected += (object sender, EventArgs e) => {
				_user.Sex = Sex.Male;
				_backImage = UIImage.FromFile ("bg.png");
				UpdateDailyProteinIntake();
				_sexImage.BackgroundImage = _backImage;
				_weightImage.BackgroundImage = _backImage;
				_levelImage.BackgroundImage = _backImage;
				_notificationImage.BackgroundImage = _backImage;
				_resetImage.BackgroundImage = _backImage;

			};
			_femaleRadioElement = new ClickableRadioElement (Sex.Female.ToString ());
			_femaleRadioElement.OnSelected += (object sender, EventArgs e) => {
				_user.Sex = Sex.Female;
				_backImage = UIImage.FromFile ("bg-female.png");
				UpdateDailyProteinIntake();
				_sexImage.BackgroundImage = _backImage;
				_weightImage.BackgroundImage = _backImage;
				_levelImage.BackgroundImage = _backImage;
				_notificationImage.BackgroundImage = _backImage;
				_resetImage.BackgroundImage = _backImage;
			};
			/***********************************/

			/******* UNITS ********/
			_metricRadioElement = new ClickableRadioElement ("Metric (kg)");
			_metricRadioElement.OnSelected += (object sender, EventArgs e) => {
				_user.IsMetric = true;
				UpdateDailyProteinIntake();
			};
			_imperialRadioElement = new ClickableRadioElement ("Imperial (lbs)");
			_imperialRadioElement.OnSelected += (object sender, EventArgs e) => {
				_user.IsMetric = false;
				UpdateDailyProteinIntake();
			};

			/******  WEIGHT ******/
			_weightElement = new EntryElementAdv ("Weight", string.Empty, _user.Weight.ToString(), UIColor.White, UIColor.Clear);
			_weightElement.KeyboardType = UIKeyboardType.DecimalPad;
			_weightElement.ReturnKeyType = UIReturnKeyType.Done;
			_weightElement.Changed += (sender, e) => 
			{
				_user.Weight = Double.Parse( _weightElement.Value);
				UpdateDailyProteinIntake();
			};

			/****** ACTIVITY LEVEL ******/
			_sedRadElement = new ClickableRadioElement ("Sedentary");
			_sedRadElement.OnSelected += (sender, e) => {
				_user.ActivityLevel = ActivityLevel.Sedentary;
				UpdateDailyProteinIntake();
			};
			_lightRadElement = new ClickableRadioElement ("Light");
			_lightRadElement.OnSelected += (sender, e) => {
				_user.ActivityLevel = ActivityLevel.Light;
				UpdateDailyProteinIntake();
			};
			_modRadElement = new ClickableRadioElement ("Moderate");
			_modRadElement.OnSelected += (sender, e) => {
				_user.ActivityLevel = ActivityLevel.Moderate;
				UpdateDailyProteinIntake();
			};
			_veryActiveRadElement = new ClickableRadioElement ("Very Active");
			_veryActiveRadElement.OnSelected += (sender, e) => {
				_user.ActivityLevel = ActivityLevel.VeryActive;
				UpdateDailyProteinIntake();
			};
			_heavyRadElement = new ClickableRadioElement ("Heavy");
			_heavyRadElement.OnSelected += (sender, e) => {
				_user.ActivityLevel = ActivityLevel.Heavy;
				UpdateDailyProteinIntake();
			};


			/******* NOTIFICATIONS ********/
			_notificationElement = new BooleanElementAdv ("Enable Notifications", _user.NotificationsEnabled, UIColor.White, UIColor.Clear);
			_notificationElement.ValueChanged += (sender, e) => {
				_user.NotificationsEnabled = _notificationElement.Value;
				ProCalManager.Instance.Save();
			};

			/******* DAILY INTAKE ********/
			_dailyIntakeElement = new EntryElementAdv ("Daily Intake (g)", "g", _user.DailyProteinIntakeLimit.ToString (), UIColor.White, UIColor.Clear);
			_dailyIntakeElement.KeyboardType = UIKeyboardType.DecimalPad;
			_dailyIntakeElement.ReturnKeyType = UIReturnKeyType.Done;
			_dailyIntakeElement.Changed += (sender, e) => 
			{
				_user.DailyProteinIntakeLimit = Math.Round( Double.Parse( _dailyIntakeElement.Value));
				ProCalManager.Instance.Save();
				ProCalManager.Instance.UpdateCalendar = true;
			};

			/* Set up elements
			 * */
			UIView header = new UIView (new RectangleF(0,0,320,90));
			UIImageView buttonsBackgroundView = new UIImageView (new RectangleF (0, 0, 320, 49));
			buttonsBackgroundView.Image = UIImage.FromFile ("add-bg.png");
			/*
			 * Email button
			 * */
			_emailButton = UIButton.FromType (UIButtonType.Custom);
			_emailButton.Frame = new RectangleF (20,10,113,29);
			_emailButton.SetImage (UIImage.FromFile ("email-btn.png"), UIControlState.Normal);
			_emailButton.SetImage (UIImage.FromFile ("email-btn-on.png"), UIControlState.Highlighted);
			_emailButton.TouchUpInside += EmailButtonOnTouch;

			/*
			 * Facebook button
			 * */
			_fbButton = UIButton.FromType (UIButtonType.Custom);
			_fbButton.Frame = new RectangleF (180,7,113,37);
			_fbButton.SetImage (UIImage.FromFile ("facebook-btn.png"), UIControlState.Normal);
			_fbButton.SetImage (UIImage.FromFile ("facebook-btn-on.png"), UIControlState.Highlighted);
			_fbButton.TouchUpInside += FacebookButtonOnTouch;

			UILabel proteinCalcHeader = new UILabel ()
			{ 
				TextColor = UIColor.White, 
				Text = "Protein Calculator", 
				Font = UIFont.BoldSystemFontOfSize(16),
				TextAlignment = UITextAlignment.Left,
				Frame = new RectangleF(10, 49, 200, 41)
			};
			header.AddSubview (buttonsBackgroundView);
			header.AddSubview (_emailButton);
			header.AddSubview (_fbButton);
			header.AddSubview (proteinCalcHeader);
			header.BringSubviewToFront (_emailButton);

			UIView footer = new UIView (new RectangleF(0,0,320,48));
			UILabel proteinCalcFooter = new UILabel ()
			{ 
				TextColor = UIColor.White, 
				Text = "You can override the calculated daily intake and enter you own protein goal.",
				Font = UIFont.SystemFontOfSize(14),
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				Frame = new RectangleF(10, 0, 310, 48)
			};
			footer.AddSubview (proteinCalcFooter);

			UIView notfSectionFooter = new UIView (new RectangleF(0,0,320,48));
			UILabel notLabel = new UILabel ()
			{ 
				TextColor = UIColor.White, 
				Text = "ProShake will notify you every so often to ensure you're on track with your protein intake.",
				Font = UIFont.SystemFontOfSize(14),
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				Frame = new RectangleF(10, 0, 310, 48)
			};
			notfSectionFooter.AddSubview (notLabel);

			UIView resetFooter = new UIView (new RectangleF(0,0,320,48));
			UILabel resetLabel = new UILabel ()
			{ 
				TextColor = UIColor.White, 
				Text = "Resetting ProShake will clear your food diary " +
				       "and all other configuration settings.",
				Font = UIFont.SystemFontOfSize(14),
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				Frame = new RectangleF(10, 0, 310, 48)
			};
			resetFooter.AddSubview (resetLabel);

			/*** BACKGROUND ROOT ELEMENTS **/
			_backImage = ProCalManager.Instance.User.Sex == Sex.Male ? UIImage.FromFile ("bg.png") : UIImage.FromFile ("bg-female.png");
			_sexImage = new BackgoundImageRootElement ("Sex", _backImage, UIColor.Clear, UIColor.White, new RadioGroup (_user.Sex == Sex.Male ? 0 : 1)) {
				new Section () {
					_maleRadioElement,
					_femaleRadioElement
				}
			};
			_weightImage = new BackgoundImageRootElement ("Weight Units", _backImage, UIColor.Clear, UIColor.White, new RadioGroup (_user.IsMetric ? 0 : 1)) {
				new Section () {
					_metricRadioElement,
					_imperialRadioElement
				}
			};
			_levelImage = new BackgoundImageRootElement ("Activity Level", _backImage, UIColor.Clear, UIColor.White, new RadioGroup ((int)_user.ActivityLevel)) {
				new Section () {
					_sedRadElement,
					_lightRadElement,
					_modRadElement,
					_veryActiveRadElement,
					_heavyRadElement
				}
			};
			_notificationImage = new BackgoundImageRootElement ("Notifications", _backImage, UIColor.Clear, UIColor.White) {
				new Section (null, notfSectionFooter) {
					_notificationElement
				}
			};
			_resetImage = new BackgoundImageRootElement ("Reset", _backImage, UIColor.Clear, UIColor.White) {
				new Section (null, resetFooter) {
					new SelectedStringElement ("Reset All Data", UITableViewCellSelectionStyle.Gray, delegate {
						Reset ();
					}, UIColor.White, UIColor.Clear)
				}
			};

			UIView disclaimerFooter = new UIView (new RectangleF(0,0, 320, 150));
			UILabel disclaimerLabel = new UILabel ()
			{ 
				TextColor = UIColor.White, 
				Text = "Always consult your physician or a professional fitness and/or nutrition expert before beginning any weight loss or exercise program. The information on ProShake is not a substitute for professional medical advice and is not meant to diagnose any health-related issues. ",
				Font = UIFont.SystemFontOfSize(14),
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				Frame = new RectangleF(10, 0, 310, 150)
			};
			disclaimerFooter.AddSubview (disclaimerLabel);
			 
			_root = new RootElement ("Settings") {
				new Section (header, footer) {
					_sexImage,
					_weightImage,
					_levelImage,
					_weightElement,
					_dailyIntakeElement
				},
				new Section () {
					_notificationImage,
					new ContextualStringElement ("Version", NSBundle.MainBundle.ObjectForInfoDictionary ("CFBundleShortVersionString").ToString (), UIColor.White, UIColor.Clear),
				},
				new Section () {
					_resetImage
				},
				new Section(null, disclaimerFooter)
				{
					new ContextualStringElement ("Disclaimer", string.Empty, UIColor.White, UIColor.Clear),
				}
			};
				
			NavigationBar.BackgroundColor = UIColor.FromRGBA (0, 0, 0, 0.2f);
			var textAttr = new UITextAttributes ();
			textAttr.TextColor = UIColor.White;
			NavigationBar.TintColor = UIColor.FromRGB (80, 169, 237);
			NavigationBar.SetTitleTextAttributes (textAttr);

			_dv = new SettingsDialogViewController (_root, false);
			PushViewController (_dv, true);

			// used to dismiss keyboard for textfields
			var tap = new UITapGestureRecognizer ();
			tap.CancelsTouchesInView = false;
			tap.AddTarget (() =>{
				_dv.View.EndEditing (true);
			});
			_dv.View.AddGestureRecognizer (tap);
		}

		private void FacebookButtonOnTouch (object sender, EventArgs e)
		{
			try
			{
				NSUrl url = new NSUrl("fb://profile/217249721810254");
				if(UIApplication.SharedApplication.CanOpenUrl(url))
				{
					UIApplication.SharedApplication.OpenUrl(url);
				}
				else{
					UIApplication.SharedApplication.OpenUrl(new NSUrl("http://www.facebook.com/proshakeapp"));
				}
			}
			catch(Exception ex) {
				Console.WriteLine (ex.Message);
				var alertView = new UIAlertView ("Error", "There was a problem opening the facebook page", null, "OK", null);
				alertView.Show ();
			}
		}

		private void EmailButtonOnTouch (object sender, EventArgs e)
		{
			MFMailComposeViewController mail = new MFMailComposeViewController();
			mail.SetToRecipients (new []{"proshake@back2frontsolutions.com"});
			mail.SetSubject ("ProShake Query");
			mail.Finished += (s, args) => 
			{
				DismissViewController(true, null);

			};
			PresentViewController (mail, true,null);
		}

		public void CloseView(bool reset)
		{
			if(Close != null)
			{
				Close(this, new SettingsCloseArgs(reset));
			}
		}

		public class ResetAlertViewDelegate : UIAlertViewDelegate
		{
			private SettingsNavController _controller;
			public ResetAlertViewDelegate (SettingsNavController controller)
			{
				_controller = controller;
			}

			public override void Clicked (UIAlertView alertview, int buttonIndex)
			{
				try
				{
					if(buttonIndex == 1)
					{
						return;
					}

					ProCalManager.Instance.Reset();
					_controller.PerformSegue("Reset", _controller);
					//_controller.CloseView(true);
				}
				catch(Exception e)
				{
					//Logger.Instance.Log(e);
					Console.WriteLine(e.Message);
				}
			}
		}

		private void Reset()
		{
			UIAlertView alert = new UIAlertView("Reset", "Are you sure?", _alertDelegate,"OK","Cancel");
			alert.Show();
		}

		private void UpdateDailyProteinIntake()
		{
			ProteinCalculator.CalculateDailyIntake (_user);
			ProCalManager.Instance.Save();
			ProCalManager.Instance.UpdateCalendar = true;
			_dailyIntakeElement.SetValueAndUpdate (_user.DailyProteinIntakeLimit.ToString());
		}
	}

	public class SettingsCloseArgs : EventArgs
	{
		public bool IsReset{ get; private set; }
		public SettingsCloseArgs(bool reset)
		{
			IsReset = reset;
		}
	}
}
