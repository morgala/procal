using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;
using Back2Front.Utilities;
using Back2Front.GUI;

namespace ProCal
{
	public partial class DragLevelView : UIView
	{
		public DragLevelView ()
		{
		}

		public DragLevelView (IntPtr handle) : base(handle)
		{
		}

		public static DragLevelView GetView()
		{
			return  NibLoader.LoadUIObject<DragLevelView>("DragLevelView");
		}

		public void Animate()
		{
			arrowImageView.Transform = CGAffineTransform.MakeTranslation (0, -10f);

			UIView.Animate (0.1f, 0.1f, 
				UIViewAnimationOptions.AllowUserInteraction | 
				UIViewAnimationOptions.Repeat | 
				UIViewAnimationOptions.Autoreverse,
				() => {
					arrowImageView.Transform = CGAffineTransform.MakeIdentity();
				}, null);
		}
	}
}

