using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Back2Front.Animation;
using Back2Front.GUI;
using Back2Front.Utilities;

namespace ProCal
{
	public partial class ProfileViewController : UIViewController
	{
		private CalendarWeekView _weekView;
		private User _user;
		private UIImageView _percentageImageView;
		private CGColor _goldColor;
		private UILabel _piePercentLabel;
		private UILabel _pieAwardLabel;
		private AnimatedPieChart _pieChart;
		private readonly List<Slice> _slices = new List<Slice>(3);

		public ProfileViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_weekView = new CalendarWeekView(new RectangleF(0, 164, 320, 100), true, true);
			_weekView.AnimateView = true;
			scrollView.AddSubview (_weekView);

			bronzeProgressView.TrackTintColor = UIColor.Black;
			silverProgressView.TrackTintColor = UIColor.Black;
			goldProgressView.TrackTintColor = UIColor.Black;

			float red, green, blue, alpha;
			ProteinAwardColors.GoldColor.GetRGBA (out red, out green, out blue, out alpha);
			_goldColor = new CGColor (red, green, blue, alpha);
			_percentageImageView = new UIImageView (UIImage.FromBundle ("pie-overlay"));
			_pieChart = new AnimatedPieChart (new RectangleF (5, 283, 183, 183));
			scrollView.AddSubview (_pieChart);
			scrollView.AddSubview (_percentageImageView);
			_percentageImageView.Center = _pieChart.PieView.Center;

			_piePercentLabel = new UILabel ();
			_piePercentLabel.Frame = new RectangleF (_percentageImageView.Center.X, _percentageImageView.Center.Y, 100, 50); 
			_piePercentLabel.Center = new PointF (_percentageImageView.Center.X, _percentageImageView.Center.Y - 10);
			_piePercentLabel.Text = "0%";
			_piePercentLabel.TextAlignment = UITextAlignment.Center;
			_piePercentLabel.Font = SystemFont.SystemFontOfSize (38);
			_piePercentLabel.TextColor = UIColor.White;
			scrollView.AddSubview (_piePercentLabel);

			_pieAwardLabel = new UILabel ();
			_pieAwardLabel.Frame = new RectangleF (_percentageImageView.Center.X, _percentageImageView.Center.Y, 100, 50); 
			_pieAwardLabel.Center = new PointF (_percentageImageView.Center.X, _percentageImageView.Center.Y + 20);
			_pieAwardLabel.Text = "GOLD";
			_pieAwardLabel.TextAlignment = UITextAlignment.Center;
			_pieAwardLabel.TextColor = UIColor.White;
			_pieAwardLabel.Font = SystemFont.SystemFontOfSize (20);
			scrollView.AddSubview (_pieAwardLabel);
			scrollView.BringSubviewToFront (staticDailyLabel);


			NSNotificationCenter.DefaultCenter.AddObserver (UIApplication.WillEnterForegroundNotification, (n) => 
			{
				try
				{
					if(DateTime.Today > ProCalManager.Instance.ResignTimeStamp.Date)
					{
						_weekView.MoveToToday();
						_weekView.RefreshWeek();
						_weekView.UpdateDateLabel(DateTime.Now);
						var lastDayOfMonth = DateTime.Now.LastDayOfMonth ();
						int daysLeft = lastDayOfMonth.Day - DateTime.Today.Day;
						numDaysLeftLabel.Text = daysLeft.ToString();
					}
				}
				catch(Exception e)
				{
					Console.WriteLine(e.Message);
				}
			});
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			_user = ProCalManager.Instance.User;
			backgroundImageView.Image = _user.Sex == Sex.Male ? UIImage.FromFile ("bg.png") : UIImage.FromFile ("bg-female.png");
			DisplayDailyProteinProgress ();
			DisplayWeeklyProteinProgress ();
			DisplayMonthProgress ();
			_weekView.MoveToToday();
			_weekView.RefreshWeek();
			_weekView.Animate ();
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			bronzeProgressView.SetProgress (0, true);
			silverProgressView.SetProgress (0, true);
			goldProgressView.SetProgress (0, true);
			_slices.Clear ();
			_pieChart.Clear ();
		}
			
		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}

		public void DisplayMonthProgress()
		{
			var firstDayOfMonth = DateTime.Now.FirstDayOfMonth();
			var lastDayOfMonth = DateTime.Now.LastDayOfMonth ();
			int daysLeft = lastDayOfMonth.Day - DateTime.Today.Day;
			int bronzeAwardCount = ProCalManager.Instance.ProteinAwards.Where (kvp => kvp.Key.Date >= firstDayOfMonth && kvp.Key.Date <= lastDayOfMonth).Count (kvp => kvp.Value == ProteinAward.Bronze);
			int silverAwardCount = ProCalManager.Instance.ProteinAwards.Where (kvp => kvp.Key.Date >= firstDayOfMonth && kvp.Key.Date <= lastDayOfMonth).Count (kvp => kvp.Value == ProteinAward.Silver);
			int goldAwardCount = ProCalManager.Instance.ProteinAwards.Where (kvp => kvp.Key.Date >= firstDayOfMonth && kvp.Key.Date <= lastDayOfMonth).Count (kvp => kvp.Value == ProteinAward.Gold);
			int total = bronzeAwardCount + silverAwardCount + goldAwardCount;

			float goldPercent = ((float)goldAwardCount / total)*100;
			float silverPercent = ((float)silverAwardCount / total)*100;
			float bronzePercent = ((float)bronzeAwardCount / total)*100;

			if (total != 0)
			{ 

				_slices.Add(new Slice{Name = "Bronze", Percentage = (float) bronzeAwardCount/total, Color = ProteinAwardColors.BronzeColor.CGColor });
				_slices.Add(new Slice{Name = "Silver", Percentage = (float) silverAwardCount/total, Color = ProteinAwardColors.SilverColor.CGColor });
				_slices.Add(new Slice{Name = "Gold", Percentage = (float) goldAwardCount/total, Color = _goldColor});
				_pieChart.Slices = _slices;
				_pieChart.Load ();
			}

			_piePercentLabel.Text = Double.IsNaN(goldPercent) ? "0%" :  Math.Round((double)goldPercent).ToString() + "%";
			silverPercentLabel.Text = Double.IsNaN(silverPercent) ? "0%" : Math.Round((double)silverPercent).ToString() + "%";
			bronzePercentLabel.Text =  Double.IsNaN(bronzePercent) ? "0%" :Math.Round((double)bronzePercent).ToString() + "%";
			numDaysLeftLabel.Text = daysLeft.ToString();
		}

		public void DisplayDailyProteinProgress()
		{
			double todaysProteinValue = _user.TodaysTotalProteinIntake;
			dailyProteinLabel.Text = string.Format("{0}/{1}", todaysProteinValue, _user.DailyProteinIntakeLimit);

			ProteinAward award = ProteinCalculator.CalculateAward (todaysProteinValue);
			switch (award) {
			case ProteinAward.Bronze:
				double bronzeProgress = todaysProteinValue / ProteinCalculator.BronzeUpperLimit;
				bronzeProgressView.SetProgress ((float)bronzeProgress, true);
				break;
			case ProteinAward.Silver:
				double silverLimit = (ProteinCalculator.GoldLowerLimit - ProteinCalculator.BronzeUpperLimit) - (ProteinCalculator.GoldLowerLimit - todaysProteinValue);
				double silverProgress = silverLimit / (ProteinCalculator.GoldLowerLimit - ProteinCalculator.BronzeUpperLimit);
				bronzeProgressView.SetProgress (1, true);
				silverProgressView.SetProgress ((float)silverProgress, true);
				break;
			case ProteinAward.Gold:
				double goldLimit = (_user.DailyProteinIntakeLimit - ProteinCalculator.GoldLowerLimit) - (_user.DailyProteinIntakeLimit - todaysProteinValue);
				double goldProgress = goldLimit / (_user.DailyProteinIntakeLimit - ProteinCalculator.GoldLowerLimit);

				bronzeProgressView.SetProgress (1, true);
				silverProgressView.SetProgress (1, true);
				if (goldProgress >= 1) {
					goldProgressView.SetProgress (1, true);
				} else {
					goldProgressView.SetProgress ((float)goldProgress, true);
				}
				break;
			}
		}

		public void DisplayWeeklyProteinProgress()
		{
			double weeklyIntake = _user.WeeklyProteinIntake;
			weeklyProteinLabel.Text = string.Format("{0}/{1}", weeklyIntake, _user.WeeklyProteinIntakeLimit);
		}
	}
}
