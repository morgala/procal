// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("SetUpSecondStageViewController")]
	partial class SetUpSecondStageViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton goButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel proteinGramsPerLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISlider proteinGramsSlider { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel proteinLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton skipButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (proteinGramsSlider != null) {
				proteinGramsSlider.Dispose ();
				proteinGramsSlider = null;
			}

			if (proteinGramsPerLabel != null) {
				proteinGramsPerLabel.Dispose ();
				proteinGramsPerLabel = null;
			}

			if (proteinLabel != null) {
				proteinLabel.Dispose ();
				proteinLabel = null;
			}

			if (goButton != null) {
				goButton.Dispose ();
				goButton = null;
			}

			if (skipButton != null) {
				skipButton.Dispose ();
				skipButton = null;
			}
		}
	}
}
