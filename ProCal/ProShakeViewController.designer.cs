// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("ProShakeViewController")]
	partial class ProShakeViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton addButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView backgroundImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView promptImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton resetButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView scaleImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel shakeLevelLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView shakeLevelView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton testButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backgroundImageView != null) {
				backgroundImageView.Dispose ();
				backgroundImageView = null;
			}

			if (addButton != null) {
				addButton.Dispose ();
				addButton = null;
			}

			if (promptImageView != null) {
				promptImageView.Dispose ();
				promptImageView = null;
			}

			if (resetButton != null) {
				resetButton.Dispose ();
				resetButton = null;
			}

			if (scaleImageView != null) {
				scaleImageView.Dispose ();
				scaleImageView = null;
			}

			if (shakeLevelLabel != null) {
				shakeLevelLabel.Dispose ();
				shakeLevelLabel = null;
			}

			if (shakeLevelView != null) {
				shakeLevelView.Dispose ();
				shakeLevelView = null;
			}

			if (testButton != null) {
				testButton.Dispose ();
				testButton = null;
			}
		}
	}
}
