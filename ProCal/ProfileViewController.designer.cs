// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ProCal
{
	[Register ("ProfileViewController")]
	partial class ProfileViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView backgroundImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel bronzePercentLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIProgressView bronzeProgressView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel dailyProteinLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIProgressView goldProgressView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel numDaysLeftLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView pieChartImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIScrollView scrollView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton settingsButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel silverPercentLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIProgressView silverProgressView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel staticDailyLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel weeklyProteinLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (backgroundImageView != null) {
				backgroundImageView.Dispose ();
				backgroundImageView = null;
			}

			if (bronzePercentLabel != null) {
				bronzePercentLabel.Dispose ();
				bronzePercentLabel = null;
			}

			if (bronzeProgressView != null) {
				bronzeProgressView.Dispose ();
				bronzeProgressView = null;
			}

			if (dailyProteinLabel != null) {
				dailyProteinLabel.Dispose ();
				dailyProteinLabel = null;
			}

			if (goldProgressView != null) {
				goldProgressView.Dispose ();
				goldProgressView = null;
			}

			if (numDaysLeftLabel != null) {
				numDaysLeftLabel.Dispose ();
				numDaysLeftLabel = null;
			}

			if (pieChartImageView != null) {
				pieChartImageView.Dispose ();
				pieChartImageView = null;
			}

			if (scrollView != null) {
				scrollView.Dispose ();
				scrollView = null;
			}

			if (settingsButton != null) {
				settingsButton.Dispose ();
				settingsButton = null;
			}

			if (silverPercentLabel != null) {
				silverPercentLabel.Dispose ();
				silverPercentLabel = null;
			}

			if (silverProgressView != null) {
				silverProgressView.Dispose ();
				silverProgressView = null;
			}

			if (staticDailyLabel != null) {
				staticDailyLabel.Dispose ();
				staticDailyLabel = null;
			}

			if (weeklyProteinLabel != null) {
				weeklyProteinLabel.Dispose ();
				weeklyProteinLabel = null;
			}
		}
	}
}
